LOCAL_PATH := ../../clone/

include $(CLEAR_VARS)

LOCAL_MODULE := Clone
LOCAL_C_INCLUDES := $(LOCAL_PATH)/include
LOCAL_CFLAGS += -O3 -DUSE_FILE32API

LOCAL_SRC_FILES := $(subst $(LOCAL_PATH)/,,$(wildcard $(LOCAL_PATH)/src/*.cpp))
LOCAL_SRC_FILES := $(LOCAL_SRC_FILES) $(subst $(LOCAL_PATH)/,,$(wildcard $(LOCAL_PATH)/src/*/*.cpp))
LOCAL_SRC_FILES := $(LOCAL_SRC_FILES) $(subst $(LOCAL_PATH)/,,$(wildcard $(LOCAL_PATH)/src/*/*/.cpp))

LOCAL_SRC_FILES := $(LOCAL_SRC_FILES) $(subst $(LOCAL_PATH)/,,$(wildcard $(LOCAL_PATH)/src/*.c))
LOCAL_SRC_FILES := $(LOCAL_SRC_FILES) $(subst $(LOCAL_PATH)/,,$(wildcard $(LOCAL_PATH)/src/*/*.c))
LOCAL_SRC_FILES := $(LOCAL_SRC_FILES) $(subst $(LOCAL_PATH)/,,$(wildcard $(LOCAL_PATH)/src/*/*/*.c))
LOCAL_SRC_FILES := $(subst src/External/glew.c,,$(LOCAL_SRC_FILES))

LOCAL_SHARED_LIBRARIES := SDL2 SDL2_image SDL2_ttf SDL2_mixer SDL2_net libstlport
LOCAL_LDLIBS := -lGLESv2 -lEGL -lz

include $(BUILD_SHARED_LIBRARY)
