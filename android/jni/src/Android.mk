LOCAL_PATH := ../src/

include $(CLEAR_VARS)

LOCAL_MODULE := main
CLONE_PATH := ../../clone/

SDL_PATH := $(CLONE_PATH)/android/jni/SDL2/
LOCAL_C_INCLUDES := $(CLONE_PATH)/include
LOCAL_CFLAGS += -O3
# Add your application source files here...
LOCAL_SRC_FILES := $(SDL_PATH)/src/main/android/SDL_android_main.c 
LOCAL_SRC_FILES := $(LOCAL_SRC_FILES) $(wildcard $(LOCAL_PATH)/*.cpp)
LOCAL_SRC_FILES := $(LOCAL_SRC_FILES) $(wildcard $(LOCAL_PATH)/*/*.cpp)
LOCAL_SRC_FILES := $(LOCAL_SRC_FILES) $(wildcard $(LOCAL_PATH)/*/*/*.cpp)

LOCAL_SHARED_LIBRARIES := Clone SDL2 SDL2_image SDL2_ttf SDL2_mixer SDL2_net
LOCAL_LDLIBS := -lGLESv1_CM -lGLESv2 -llog -lEGL

include $(BUILD_SHARED_LIBRARY)
