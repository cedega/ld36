#include "ShaderManager.hpp"
#include <Clone/Core.hpp>
#include <Clone/Graphics.hpp>

cl::Shader *ShaderManager::shader2d = NULL;
cl::Shader *ShaderManager::shader3d = NULL;
cl::Shader *ShaderManager::particles = NULL;
cl::Shader *ShaderManager::debug = NULL;

void ShaderManager::initialize()
{
    cl::AssetLoader loader;
    cl::File *debug_vs = loader.loadFile("data/shaders/debug_vs.glsl", cl::AssetLoader::None, NULL);
    cl::File *debug_fs = loader.loadFile("data/shaders/debug_fs.glsl", cl::AssetLoader::None, NULL);

    cl::File *image_vs = loader.loadFile("data/shaders/image_vs.glsl", cl::AssetLoader::None, NULL);
    cl::File *image_fs = loader.loadFile("data/shaders/image_fs.glsl", cl::AssetLoader::None, NULL);

    cl::File *model_vs = loader.loadFile("data/shaders/model_static_vs.glsl", cl::AssetLoader::None, NULL);
    cl::File *model_fs = loader.loadFile("data/shaders/model_static_fs.glsl", cl::AssetLoader::None, NULL);

    cl::File *particle_vs = loader.loadFile("data/shaders/particle_emitter_vs.glsl", cl::AssetLoader::None, NULL);
    cl::File *particle_fs = loader.loadFile("data/shaders/particle_emitter_fs.glsl", cl::AssetLoader::None, NULL);

    shader2d = cl::GraphicsAPI::getInstance()->createShader();
    shader3d = cl::GraphicsAPI::getInstance()->createShader();
    particles = cl::GraphicsAPI::getInstance()->createShader();
    debug = cl::GraphicsAPI::getInstance()->createShader();

    shader2d->compileVertexShader(image_vs, true);
    shader2d->compileFragmentShader(image_fs, true);
    shader2d->bindAttribLocation("vPosition", cl::VertexPosition);
    shader2d->bindAttribLocation("vTexCoord", cl::VertexTexCoord0);
    shader2d->linkProgram();
    shader2d->setUniformI("tex_sample", 0);

    shader3d->compileVertexShader(model_vs, true);
    shader3d->compileFragmentShader(model_fs, true);
    shader3d->bindAttribLocation("vPosition", cl::VertexPosition);
    shader3d->bindAttribLocation("vTexCoord", cl::VertexTexCoord0);
    shader3d->linkProgram();
    shader3d->setUniformI("tex_sample", 0);

    particles->compileVertexShader(particle_vs, true);
    particles->compileFragmentShader(particle_fs, true);
    particles->bindAttribLocation("vPosition", cl::VertexPosition);
    particles->bindAttribLocation("vLastPosition", cl::VertexLastPosition);
    particles->bindAttribLocation("vTexCoord", cl::VertexTexCoord0);
    particles->linkProgram();

    debug->compileVertexShader(debug_vs, true);
    debug->compileFragmentShader(debug_fs, true);
    debug->bindAttribLocation("vPosition", cl::VertexPosition);
    debug->linkProgram();

    delete debug_vs;
    delete debug_fs;
    delete image_vs;
    delete image_fs;
    delete model_vs;
    delete model_fs;
    delete particle_vs;
    delete particle_fs;
}

void ShaderManager::destroy()
{
    delete shader2d;
    delete shader3d;
    delete particles;
    delete debug;
}