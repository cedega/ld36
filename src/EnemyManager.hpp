#pragma once
#include "Enemy.hpp"
#include <Clone/Core.hpp>

class EnemyManager
{
public:
	static Enemy* addEnemy(Enemy* e);
	static void clear();
	static std::vector<Enemy*>& getEnemies();
	static void initEnemies(cl::AssetCache* cache);
private: 
	static std::vector<Enemy*> enemies_;
	EnemyManager();
	~EnemyManager();
	static EnemyManager* instance_;
};
