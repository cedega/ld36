#include "Entity.hpp"

bool Entity::disabled_ = false;

Entity::Entity() :
    collisions_(NULL),
    speed_(0.0f),
    jump_height_(0.0f),
    max_health_(1),
    health_(1),
    hurt_tick_(100.0f),
    is_stunnable_(true)
{
    setCullAABB(cl::AABB3D(glm::vec3(0.0f), glm::vec3(3.0f)));
}

Entity::~Entity()
{
    for (unsigned int i = 0; i < projectiles_.size(); i++)
        delete projectiles_[i];
}

void Entity::update(float dt)
{
    cl::AnimatedSpriteSheet::updateLastState();

    glm::vec3 velocity;
    velocity.y = physics_.getVelocity().y;
    hurt_tick_ += dt;

    if (isPressingLeft())
    {
        velocity.x -= speed_;

        if (getScaling().x > 0.0f && isAlive())
            setScaling(glm::vec3(-1.0f, 1.0f, 1.0f) * getScaling(), true);
    }

    if (isPressingRight())
    {
        velocity.x += speed_;

        if (getScaling().x < 0.0f && isAlive())
            setScaling(glm::vec3(-1.0f, 1.0f, 1.0f) * getScaling(), true);
    }

    if (hurt_tick_ < 0.2f && is_stunnable_)
        velocity.x = 0.0f;

    physics_.setVelocity(velocity);

    if (disabled_ || !isAlive())
        physics_.setVelocity(physics_.getVelocity() * glm::vec3(0.0f, 1.0f, 0.0f));

    if (isPressingJump() && !physics_.isJumping() && !isDisabled() && isAlive())
    {
        physics_.setJumpVelocity(glm::vec3(0.0f, jump_height_, 0.0f));
        physics_.setVelocity(physics_.getVelocity() * glm::vec3(1.0f, 0.0f, 1.0f));
        physics_.jump();
    }

    physics_.integrate(dt);
    hitbox_.centre = physics_.getPosition();

    for (unsigned int i = 0; i < collisions_->getOBBs().size(); i++)
    {
        glm::vec3 mtv;
        if (cl::intersect3DSAT(hitbox_, collisions_->getOBBs()[i], &mtv))
        {
            hitbox_.centre += mtv;

            if (glm::dot(glm::vec3(0.0f, 1.0f, 0.0f), glm::normalize(mtv)) > 0.5f)
            {
                if (physics_.getVelocity().y < 0.1f)
                {
                    physics_.setVelocity(physics_.getVelocity() * glm::vec3(1.0f, 0.0f, 1.0f));
                    physics_.land();
                }

            }
            else if (glm::dot(glm::vec3(0.0f, -1.0f, 0.0f), glm::normalize(mtv)) > 0.5f)
            {
                if (physics_.getVelocity().y > 0.1f)
                {
                    physics_.setVelocity(physics_.getVelocity() * glm::vec3(1.0f, 0.0f, 1.0f));
                }
            }
        }
    }

    physics_.setPosition(hitbox_.centre);
    cl::AnimatedSpriteSheet::setPosition(hitbox_.centre + hitbox_offset_ + anim_offset_, false);

    processAnimations();

    cl::AnimatedSpriteSheet::update(dt);
}

void Entity::setCollisions(Collisions *collisions)
{
    collisions_ = collisions;
}

void Entity::setPosition(const glm::vec3 &pos)
{
    physics_.setPosition(pos);
    cl::AnimatedSpriteSheet::setPosition(pos + hitbox_offset_ + anim_offset_, true);
}

glm::vec3 Entity::getPhysicsPosition() const
{
    return physics_.getPosition();
}

void Entity::setSpeed(float speed)
{
    speed_ = speed;
}

float Entity::getSpeed() const
{
    return speed_;
}

void Entity::setJump(float jump)
{
    jump_height_ = jump;
}

float Entity::getJump() const
{
    return jump_height_;
}

void Entity::setHitboxOffset(const glm::vec3 &offset)
{
    hitbox_offset_ = offset;
}

const glm::vec3& Entity::getHitboxOffset() const
{
    return hitbox_offset_;
}

void Entity::setAnimOffset(const glm::vec3 &offset)
{
    anim_offset_ = offset;
}

const glm::vec3& Entity::getAnimOffset() const
{
    return anim_offset_;
}

cl::EulerPhysics& Entity::getPhysics()
{
    return physics_;
}

const cl::OBB3D& Entity::getHitbox() const
{
    return hitbox_;
}

void Entity::setHealth(int health)
{
    health_ = health;
}

int Entity::getHealth() const
{
    return health_;
}

void Entity::setMaxHealth(int max_health)
{
    max_health_ = max_health;
}

int Entity::getMaxHealth() const
{
    return max_health_;
}

bool Entity::isAlive()
{
    return health_ > 0;
}

void Entity::damage(int damage)
{
	bool was_alive = isAlive();
    health_ -= damage;
    hurt_tick_ = 0.0f;

	if (health_ <= 0)
	{
		health_ = 0;
		onDeath();
	}
}

void Entity::setDisabled(bool b)
{
    disabled_ = b;
}

bool Entity::isDisabled()
{
    return disabled_;
}