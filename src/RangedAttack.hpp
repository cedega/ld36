#pragma once
#include "EnemyBehaviourStrategy.hpp"

class RangedAttack : public EnemyBehaviourStrategy
{
public:
	void move(Enemy& e, Player& p);
	RangedAttack();
};
