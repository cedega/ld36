#pragma once
#include <Clone/Core.hpp>
#include <Clone/Graphics.hpp>
#include "Collisions.hpp"
#include "Map.hpp"
#include "Projectile.hpp"
#include "MainMenu.hpp"
#include "UI.hpp"

class Engine : public cl::Engine
{
 public:
    Engine();
    ~Engine();

    void events(const SDL_Event &event);
    void logic(float dt);
    void render(cl::Renderer *renderer);

 private:
    Engine(const Engine &obj);
    Engine& operator=(const Engine &obj);

    Map map_;
    Player player_;
    cl::Camera camera_;
    cl::Camera camera2d_;
    cl::AssetCache cache_;
    MainMenu main_menu_;
    UI ui_;
    cl::Image win_image_;
    float fade_tick_;
    bool fullscreen_pressed_;
};
