#include "Map.hpp"
#include "Upgrades.hpp"
#include "levels/LevelDebug.hpp"
#include "levels/LevelOne.hpp"
#include "levels/LevelTwo.h"
#include "levels/LevelUpgrade.hpp"
#include "levels/LevelThree.hpp"
#include "levels/LevelFour.hpp"
#include "levels/LevelStart.hpp"
#include "levels/LevelFive.hpp"

Player *Map::player_ = NULL;
#define FADE_TIME 1.0f

Map::Map() :
    current_level_(NULL),
    tran_state_(FadingIn),
    fade_tick_(0.0f),
    black_(NULL),
    reseting_(false),
    final_level_(false)
{

}

Map::~Map()
{
    if (current_level_ != NULL)
        delete current_level_;

    if (black_ != NULL)
        delete black_;
}

void Map::initialize()
{
    fade_camera_.setProjectionMatrix(glm::ortho(-256.0f, 256.0f, -256.0f, 256.0f, -1.0f, 1.0f));
    fade_camera_.snapTo(glm::vec3(0.0f));
    cl::AssetLoader loader;

    black_ = loader.loadTexture("data/black.png", cl::AssetLoader::TextureArguments(), NULL);
    fade_image_.initialize(black_, ShaderManager::shader2d, true);
    fade_image_.setPosition(glm::vec3(0.0f), true);
    loadLevel(Level::LevelStart);
}

void Map::loadLevel(Level::Enum level)
{
    if (current_level_ != NULL)
        delete current_level_;

    EnemyManager::clear();

    setEnemyScaling(level);

    switch (level)
    {
    case Level::Debug: current_level_ = new LevelDebug(); break;
	case Level::LevelStart: current_level_ = new LevelStart(); break;
    case Level::LevelOne: current_level_ = new LevelOne(); break;
	case Level::LevelTwo: current_level_ = new LevelTwo(); break;
	case Level::LevelUpgrade: current_level_ = new LevelUpgrade(); break;
	case Level::LevelThree: current_level_ = new LevelThree(); break;
	case Level::LevelFour: current_level_ = new LevelFour(); break;
    case Level::LevelFive: current_level_ = new LevelFive(); final_level_ = true; break;
    default: current_level_ = NULL; break;
    }

    Level::PreviousLevel = level;

	player_->resetKillNumber();
}

void Map::update(float dt)
{
    current_level_->update(dt);

    if (!player_->isAlive())
        reseting_ = true;

    if (reseting_)
    {
        if (tran_state_ == None)
        {
            tran_state_ = FadingOut;
            fade_tick_ = 0.0f;
        }
        else if (tran_state_ == FadingOut && fade_tick_ >= FADE_TIME)
        {
            Upgrades::removeAll();
            player_->reset();
            loadLevel(Level::LevelStart);
            tran_state_ = FadingIn;
            fade_tick_ = 0.0f;
            Entity::setDisabled(true);
            SoundManager::stopMusic();
            SoundManager::playMusic(SoundManager::Sand, 15);
            final_level_ = false;
        }
        else if (tran_state_ == FadingIn && fade_tick_ >= FADE_TIME)
        {
            reseting_ = false;
            Entity::setDisabled(false);
        }

        fade_tick_ += dt;

        return;
    }

    Level::Enum next_level = current_level_->getNextArea();

    if (next_level != Level::Stay && tran_state_ == None)
    {
        Entity::setDisabled(true);
        tran_state_ = FadingOut;
        fade_tick_ = 0.0f;
    }

    if (tran_state_ == FadingOut)
    {
        fade_tick_ += dt;

        if (fade_tick_ >= FADE_TIME)
        {
            // Process actual load
            loadLevel(next_level);
            tran_state_ = FadingIn;
            fade_tick_ = 0.0f;
        }
    }

    if (tran_state_ == FadingIn)
    {
        fade_tick_ += dt;

        if (fade_tick_ >= FADE_TIME)
        {
            tran_state_ = None;
            Entity::setDisabled(false);
        }
    }
}

void Map::render(cl::Renderer *renderer)
{
    current_level_->render(renderer);
	current_level_->canProgress();
}

void Map::renderFade(cl::Renderer *renderer)
{
    if (tran_state_ == FadingIn || tran_state_ == FadingOut)
    {
        renderer->process();

        fade_camera_.updateViewMatrix(1.0f);
        renderer->setCamera(&fade_camera_);

        float alpha = tran_state_ == FadingIn ? 1.0f - (fade_tick_ / FADE_TIME) : fade_tick_ / FADE_TIME;
        fade_image_.setColour(glm::vec4(1.0f, 1.0f, 1.0f, alpha));
        renderer->draw(&fade_image_);
    }
}

bool Map::hasWon()
{
    if (!final_level_)
        return false;

    return !EnemyManager::getEnemies()[0]->isAlive();
}

void Map::setEnemyScaling(Level::Enum e)
{
    switch (e)
    {
    case Level::LevelStart: Enemy::setStatScale(1.0f); break;
    case Level::LevelOne: Enemy::setStatScale(1.5f); break;
	case Level::LevelTwo: Enemy::setStatScale(2.0f); break;
	case Level::LevelThree: Enemy::setStatScale(1.4f); break;
	case Level::LevelFour: Enemy::setStatScale(2.0f); break;
    case Level::LevelFive: Enemy::setStatScale(1.0f); break;
    default: Enemy::setStatScale(1.0f); break;
    }
}