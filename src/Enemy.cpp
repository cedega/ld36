#include "Enemy.hpp"
#include "ShaderManager.hpp"
#include "EnemyBehaviourStrategy.hpp"
#include "SoundManager.hpp"


float Enemy::scaling_ = 1.0f;

Enemy::Enemy(const glm::vec3& pos, Player& p) :
	Entity(),
	player_(p),
    attack_damage_(0.0f)
{
	setPosition(pos);
	sound_is_not_playing_ = true;
	death_sound_played_ = false;
}

Enemy::Enemy(Player& p) :
	Entity(),
	player_(p),
    attack_damage_(0.0f)
{
	setPosition(glm::vec3(0.0f, 0.0f, 0.0f));
	sound_is_not_playing_ = true;
	death_sound_played_ = false;
}

void Enemy::setPosition(const glm::vec3& pos)
{
	physics_.setPosition(pos);
}


Enemy::~Enemy()
{
	for (std::vector<EnemyBehaviourStrategy*>::iterator s = strategies_.begin(); s != strategies_.end(); ++s)
		delete *s;
}

void Enemy::setCollisions(Collisions *collisions)
{
	collisions_ = collisions;
}

EnemyBehaviourStrategy* Enemy::getStrategy(state s)
{
	if (s == state::STAND)
		return NULL;
	for (std::vector<EnemyBehaviourStrategy*>::iterator itr = strategies_.begin(); itr != strategies_.end(); ++itr)
	{
		if ((*itr)->state_ == s)
		{
			return *itr;
		}
	}
	return NULL;
}

cl::EulerPhysics& Enemy::getPhysics()
{
	return physics_;
}

void Enemy::initialize(cl::AssetCache *cache)
{
}

void Enemy::update(float dt)
{
	// set direction 
	if (strategy_ != NULL)
	{
		strategy_->move(*this, player_);
	}

    if (!isAlive())
    {
        input_.left = false;
        input_.right = false;
        setSpeed(0.0f);
        setJump(0.0f);
    }

	// dank collision memes
	damageTick_ += dt;
	if (!isDisabled() && damageTick_ > 1.0 && isAlive())
	{
        cl::OBB3D hit;
        if (getAttackBox(&hit))
        {
            if (cl::intersect3DSAT(player_.getHitbox(), hit, NULL))
            {
                playAttackSound();
                player_.damage(static_cast<int>(attack_damage_));
                damageTick_ = 0.0f;
            }
        }

		// Play the appriate enemy hit sound based on damage level. Dogs have damage of 12.5 etc...

        if (cl::intersect3DSAT(hitbox_, player_.getHitbox(), NULL))
        {
            // Dog
            playAttackSound();

            player_.damage(static_cast<int>(damage_));
            damageTick_ = 0.0f;
        }
	}

	Entity::update(dt);
}

void Enemy::onDeath()
{
	player_.incrementKillNumber();
}

void Enemy::setStrategy(state s)
{
	strategy_ = getStrategy(s);
}

void Enemy::setPlayer(const Player& p)
{
	player_ = p;
}

void Enemy::addStrategy(EnemyBehaviourStrategy* strat)
{
	strategies_.push_back(strat);
}

void Enemy::processAnimations()
{
    if (isAnimating("death"))
    {

    }
    else if (!isAlive())
    {
        cl::AnimatedSpriteSheet::loadAnimation("death");
    }
    else if (hurt_tick_ < 0.2f)
    {
        cl::AnimatedSpriteSheet::loadAnimation("hit");
    }
    else
    {
        if (!isAnimating("run"))
            cl::AnimatedSpriteSheet::loadAnimation("run");
    }
}

void Enemy::setStatScale(float scale)
{
    scaling_ = scale;
}

float Enemy::getStatScale()
{
    return scaling_;
}