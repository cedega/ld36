#pragma once
#include "EnemyBehaviourStrategy.hpp"

class ChaseStrategy : public EnemyBehaviourStrategy
{
public:
	void move(Enemy& e, Player& p);
	ChaseStrategy();
};