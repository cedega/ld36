#include <Clone/Core.hpp>
#include "Tile.hpp"
#include "ShaderManager.hpp"

Tile::Tile()
{

}

Tile::Tile(const Tile &obj) :
    cl::Model(obj),
    collisions_(obj.collisions_)
{

}

Tile::~Tile()
{

}

Tile& Tile::operator=(const Tile &obj)
{
    cl::Model::operator=(obj);
    collisions_ = obj.collisions_;
    return *this;
}

void Tile::initialize(const std::string &file_name, cl::AssetCache *cache)
{
    cl::AssetLoader loader;

    cl::AssetLoader::TextureArguments texargs;
    texargs.loader_args = cl::AssetLoader::IR | cl::AssetLoader::NoNormals;
    texargs.mag_filter = cl::Texture::Linear;
    texargs.min_filter = cl::Texture::LinearMipmapLinear;
    texargs.generate_mipmaps = true;
    cl::ModelData *model_data = loader.loadModelData("data/tiles/" + file_name, texargs, cache);

    std::string ccf_file = "data/tiles/" + file_name.substr(0, file_name.find_last_of('.')) + ".ccf";
    cl::File *ccf = loader.loadFile(ccf_file, cl::AssetLoader::None, NULL);
    collisions_.load(ccf->asString(), getPosition());
    delete ccf;

    cl::Model::setBackfaceCulling(false);
    cl::Model::initialize(model_data, ShaderManager::shader3d);
}

const std::vector<cl::OBB3D>& Tile::getCollisions() const
{
    return collisions_.getOBBs();
}