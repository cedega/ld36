#include "Upgrades.hpp"
#include "ShaderManager.hpp"

bool Upgrades::upgrades_[MaxUpgrades];
cl::Image *Upgrades::upgrade_images_[MaxUpgrades];
cl::Image *Upgrades::select_;

void Upgrades::initialize(cl::AssetCache *cache)
{
    for (int i = 0; i < MaxUpgrades; i++)
    {
        upgrades_[i] = false;
        upgrade_images_[i] = new cl::Image();
    }

    select_ = new cl::Image();

    cl::Texture *texture;
    cl::AssetLoader loader;
    cl::AssetLoader::TextureArguments texargs;
    texargs.mag_filter = cl::Texture::Linear;
    texargs.min_filter = cl::Texture::Linear;

    texture = loader.loadTexture("data/upgrades/increasehealth.png", texargs, cache);
    upgrade_images_[Health]->initialize(texture, ShaderManager::shader2d, false);
    texture = loader.loadTexture("data/upgrades/speedincrease.png", texargs, cache);
    upgrade_images_[MoveSpeed]->initialize(texture, ShaderManager::shader2d, false);
    texture = loader.loadTexture("data/upgrades/damageup.png", texargs, cache);
    upgrade_images_[Damage]->initialize(texture, ShaderManager::shader2d, false);
    texture = loader.loadTexture("data/upgrades/multishot.png", texargs, cache);
    upgrade_images_[MultiShot]->initialize(texture, ShaderManager::shader2d, false);
    texture = loader.loadTexture("data/upgrades/attackspeed.png", texargs, cache);
    upgrade_images_[AttackSpeed]->initialize(texture, ShaderManager::shader2d, false);

    texture = loader.loadTexture("data/upgrades/greaterincreasehealth.png", texargs, cache);
    upgrade_images_[GreaterHealth]->initialize(texture, ShaderManager::shader2d, false);
    texture = loader.loadTexture("data/upgrades/greaterspeedincrease.png", texargs, cache);
    upgrade_images_[GreaterMoveSpeed]->initialize(texture, ShaderManager::shader2d, false);
    texture = loader.loadTexture("data/upgrades/greaterdamageup.png", texargs, cache);
    upgrade_images_[GreaterDamage]->initialize(texture, ShaderManager::shader2d, false);
    texture = loader.loadTexture("data/upgrades/greatermultishot.png", texargs, cache);
    upgrade_images_[GreaterMultiShot]->initialize(texture, ShaderManager::shader2d, false);
    texture = loader.loadTexture("data/upgrades/greaterattackspeed.png", texargs, cache);
    upgrade_images_[GreaterAttackSpeed]->initialize(texture, ShaderManager::shader2d, false);

    texture = loader.loadTexture("data/upgrades/select.png", texargs, cache);
    select_->initialize(texture, ShaderManager::shader2d, false);

    for (int i = 0; i < MaxUpgrades; i++)
    {
        upgrade_images_[i]->setTextureRect(glm::vec4(0, 0, 428, 143));
        upgrade_images_[i]->setScaling(glm::vec3(0.01f), true);
    }

    select_->setTextureRect(glm::vec4(0, 0, 428, 143));
    select_->setScaling(glm::vec3(0.01f), true);
}

void Upgrades::destroy()
{
    for (int i = 0; i < MaxUpgrades; i++)
        delete upgrade_images_[i];

    delete select_;
}

void Upgrades::add(Upgrades::Power power)
{
    upgrades_[power] = true;
}

void Upgrades::draw(cl::Renderer *renderer, Upgrades::Power power, const glm::vec3 &position)
{
    upgrade_images_[power]->setPosition(position, true);
    renderer->draw(upgrade_images_[power]);
}

void Upgrades::drawSelect(cl::Renderer *renderer, Upgrades::Power power)
{
    select_->setPosition(upgrade_images_[power]->getPosition(), true);
    renderer->draw(select_);
}

bool Upgrades::isUpgraded(Upgrades::Power power)
{
    return upgrades_[power];
}

void Upgrades::removeAll()
{
    for (int i = 0; i < MaxUpgrades; i++)
        upgrades_[i] = false;
}