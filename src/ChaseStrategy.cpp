#include "ChaseStrategy.hpp"

ChaseStrategy::ChaseStrategy()
{
	state_ = state::STRAT_CHASE;
}

void ChaseStrategy::move(Enemy& e, Player& p)
{
	glm::vec3 enemyPos = e.getPosition();
	glm::vec3 playerPos = p.getPosition();
	float delta = 1.0f;
	if (glm::abs(enemyPos.x - playerPos.x) > delta && glm::abs(enemyPos.x - playerPos.x)<16.0f && glm::abs(enemyPos.y - playerPos.y)<10.0f)
	{ 
		if (enemyPos.x > playerPos.x)
		{
			e.input_.left = true;
			e.input_.right = false;
		}
		else
		{
			e.input_.left = false;
			e.input_.right = true;
		}

	}
	float jumpDelta = e.getJump();
	if (glm::abs(playerPos.x - enemyPos.x) < delta && glm::abs(playerPos.y - enemyPos.y) < jumpDelta)
	{
		if (!e.getPhysics().isJumping() && (playerPos.y - enemyPos.y > delta))
		{
			e.input_.up = true;
		}
		else
		{
			e.input_.up = false;
		}
	}
}