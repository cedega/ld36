#include "BossStrategy.hpp"
#include "SoundManager.hpp"

BossStrategy::BossStrategy()
{
	state_ = state::STRAT_CHASE;
}

void BossStrategy::move(Enemy& e, Player& p)
{
    if (e.isAnimating("death"))
    {
        return;
    }
    else if (!e.isAlive())
    {
        e.loadAnimation("death");
        return;
    }

	glm::vec3 enemyPos = e.getPosition();
	glm::vec3 playerPos = p.getPosition();
    float distance = glm::length(playerPos - enemyPos);
    float distanceX = glm::abs(playerPos.x - enemyPos.x);
    float distanceY = playerPos.y - enemyPos.y;
    
    if (distanceX > 6.2f && !e.isAnimating("attack"))
    {
        if (enemyPos.x > playerPos.x)
        {
            e.input_.left = true;
            e.input_.right = false;
        }
        else
        {
            e.input_.left = false;
            e.input_.right = true;
        }

        if (!e.getPhysics().isJumping() && (playerPos.y - enemyPos.y > -4.3f))
        {
            e.input_.up = true;
        }
        else
        {
            e.input_.up = false;
        }

        if (e.getPhysics().isJumping())
        {
            e.loadAnimation("jump");
        }
        else if (!e.isAnimating("run"))
        {
            e.loadAnimation("run");
            e.setSpeed(3.0f);
        }
    }
    else
    {
        if (e.getAnimator().getTick() >= e.getAnimator().getDuration() && e.isAnimating("attack") && !e.isAnimating("run"))
        {
            e.loadAnimation("run");
            if (distanceX > 5.0f)
                e.setSpeed(3.0f);
        }
        else if (!e.isAnimating("attack"))
        {
            e.loadAnimation("attack");
            SoundManager::playSound(SoundManager::BossAttack, 50);
            e.setSpeed(0.0f);

            if (enemyPos.x > playerPos.x)
            {
                e.input_.left = true;
                e.input_.right = false;
            }
            else
            {
                e.input_.left = false;
                e.input_.right = true;
            }
        }
    }
}