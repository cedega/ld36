#pragma once
#include <Clone/Core.hpp>
#include <Clone/2D.hpp>

class Upgrades
{
public:
    enum Power
    {
        Health = 0,
        MoveSpeed,
        Damage,
        MultiShot,
        AttackSpeed,

        GreaterHealth,
        GreaterMoveSpeed,
        GreaterDamage,
        GreaterMultiShot,
        GreaterAttackSpeed,

        MaxUpgrades
    };

    static void initialize(cl::AssetCache *cache);
    static void destroy();
    static void add(Upgrades::Power power);
    static void draw(cl::Renderer *renderer, Upgrades::Power power, const glm::vec3 &position);
    static void drawSelect(cl::Renderer *renderer, Upgrades::Power power);
    static bool isUpgraded(Upgrades::Power power);
    static void removeAll();

private:
    Upgrades() {}
    static bool upgrades_[MaxUpgrades];
    static cl::Image *upgrade_images_[MaxUpgrades];
    static cl::Image *select_;
};