#include "Player.hpp"
#include "ShaderManager.hpp"
#include "Upgrades.hpp"
#include <Clone/Core.hpp>
#include <iostream>
Player::Player() :
    last_attack_frame_(0),
    attack_(false),
    upgrade_select_index_(0),
	kill_number_(0)
{
    last_inputs_.shoot = true;
    inputs_.shoot = true;
}

void Player::initialize(cl::AssetCache *cache)
{
    cl::AssetLoader loader;
    cl::AssetLoader::TextureArguments texargs;
    texargs.loader_args = cl::AssetLoader::IR;
    texargs.min_filter = cl::Texture::Linear;
    texargs.mag_filter = cl::Texture::Linear;
    cl::Texture *texture = loader.loadTexture("data/aria/Upperbodyfinal.png", texargs, cache);
    cl::AnimatedSpriteSheet::initialize(texture, ShaderManager::shader2d, false, glm::ivec2(128, 128), 2);

    cl::AnimatedSpriteSheet::createAnimation("idle", 22, 30, 75 / 1000.0f);
    cl::AnimatedSpriteSheet::createAnimation("walk", 1, 12, 50 / 1000.0f);
    cl::AnimatedSpriteSheet::createAnimation("hit", 31, 31, 100 / 1000.0f);
    cl::AnimatedSpriteSheet::createAnimation("death", 32, 38, 100 / 1000.0f);
    cl::AnimatedSpriteSheet::createAnimation("jump", 13, 13, 100 / 1000.0f);
    cl::AnimatedSpriteSheet::createAnimation("fall", 14, 14, 100 / 1000.0f);
    cl::AnimatedSpriteSheet::createAnimation("attack", 15, 21, 50 / 1000.0f);

    cl::AnimatedSpriteSheet::setLooping("jump", false);
    cl::AnimatedSpriteSheet::setLooping("fall", false);
    cl::AnimatedSpriteSheet::setLooping("hit", false);
    cl::AnimatedSpriteSheet::setLooping("death", false);

    cl::AnimatedSpriteSheet::loadAnimation("idle");
    cl::AnimatedSpriteSheet::setScaling(glm::vec3(0.03f * 0.65f), true);

    cl::Texture *lower_body_texture = loader.loadTexture("data/aria/Lowerbodyfinal.png", texargs, cache);
    lower_body_.initialize(lower_body_texture, ShaderManager::shader2d, false, glm::ivec2(128, 128), 2);
    lower_body_.createAnimation("walk", 1, 12, 50 / 1000.0f);
    lower_body_.createAnimation("jump", 13, 13, 100 / 1000.0f);
    lower_body_.createAnimation("fall", 14, 14, 100 / 1000.0f);
    lower_body_.createAnimation("idle", 15, 21, 50 / 1000.0f);
    lower_body_.setLooping("jump", false);
    lower_body_.setLooping("fall", false);

    lower_body_.loadAnimation("idle");
    lower_body_.setScaling(glm::vec3(0.03f * 0.65f), true);

    Entity::setMaxHealth(75);
    Entity::setHealth(75);
    Entity::setSpeed(9.9f);
    Entity::setJump(16.5f);
    Entity::setHitboxOffset(glm::vec3(0.0f, 0.46f, 0.0f));
    hitbox_.radius = glm::vec3(0.25f, 0.8f, 0.25f);

	played_death_sound_ = false;
	sound_is_not_playing_ = true;
}

void Player::input()
{
    last_inputs_ = inputs_;

    cl::Keys keys = cl::Input::getKeyboardState();
    inputs_.up = keys[CL_KEY_UP] >= 1;
    inputs_.down = keys[CL_KEY_DOWN] >= 1;
    inputs_.left = keys[CL_KEY_LEFT] >= 1;
    inputs_.right = keys[CL_KEY_RIGHT] >= 1;
    inputs_.jump = keys[CL_KEY_X] >= 1 || keys[CL_KEY_SPACE] >= 1;
    inputs_.shoot = keys[CL_KEY_Z] >= 1;

    if (!cl::Input::getControllers()->empty())
    {
        SDL_GameController *pad = cl::Input::getControllers()->at(0);
        glm::vec2 axis = cl::Input::getControllerLeftAxis(pad);

        inputs_.up |= axis.y > 0.2f || cl::Input::getControllerButton(pad, SDL_CONTROLLER_BUTTON_DPAD_UP);
        inputs_.down |= axis.y < -0.2f || cl::Input::getControllerButton(pad, SDL_CONTROLLER_BUTTON_DPAD_DOWN);
        inputs_.left |= axis.x < -0.2f || cl::Input::getControllerButton(pad, SDL_CONTROLLER_BUTTON_DPAD_LEFT);
        inputs_.right |= axis.x > 0.2f || cl::Input::getControllerButton(pad, SDL_CONTROLLER_BUTTON_DPAD_RIGHT);
        inputs_.jump |= cl::Input::getControllerButton(pad, SDL_CONTROLLER_BUTTON_A);
        inputs_.shoot |= cl::Input::getControllerButton(pad, SDL_CONTROLLER_BUTTON_X);
    }
}

bool Player::inputUpgrades()
{
    if (inputs_.up && !last_inputs_.up)
    {
        upgrade_select_index_ -= 1;
        if (upgrade_select_index_ < 0)
            upgrade_select_index_ = 2;
		SoundManager::playSound(SoundManager::Select);
    }
    else if (inputs_.down && !last_inputs_.down)
    {
        upgrade_select_index_ += 1;
        if (upgrade_select_index_ > 2)
            upgrade_select_index_ = 0;
		SoundManager::playSound(SoundManager::Select);
    }


    //cl::Log(cl::CL_LOG_INFO, "%i", upgrade_select_index_);

    return inputs_.shoot && !last_inputs_.shoot;
}

void Player::update(float dt)
{
    input();

    Entity::update(dt);

    lower_body_.cl::AnimatedSpriteSheet::setPosition(cl::AnimatedSpriteSheet::getPosition(), false);
    lower_body_.cl::AnimatedSpriteSheet::setLastPosition(cl::AnimatedSpriteSheet::getLastPosition());

    if (lower_body_.getScaling().x > 0.0f && getScaling().x < 0.0f)
        lower_body_.setScaling(glm::vec3(-1.0f, 1.0f, 1.0f) * lower_body_.getScaling(), true);
    else if (lower_body_.getScaling().x < 0.0f && getScaling().x > 0.0f)
        lower_body_.setScaling(glm::vec3(-1.0f, 1.0f, 1.0f) * lower_body_.getScaling(), true);

    lower_body_.update(dt);
    
    if (hurt_tick_ < 0.2f && isAlive())
    {
        cl::AnimatedSpriteSheet::setColour(glm::vec4(1.0f, 0.1f, 0.1f, 1.0f));
        lower_body_.setColour(glm::vec4(1.0f, 0.1f, 0.1f, 1.0f));

		if (sound_is_not_playing_)
		{
			SoundManager::playSound(SoundManager::EnemyHit, 50);
			sound_is_not_playing_ = false;
		}
    }
    else
    {
		sound_is_not_playing_ = true;
        cl::AnimatedSpriteSheet::setColour(glm::vec4(1.0f));
        lower_body_.setColour(glm::vec4(1.0f));
    }
}

void Player::render(cl::Renderer *renderer)
{
    if (isAnimating("attack"))
        renderer->draw(&lower_body_);

    renderer->draw(this);
}

void Player::reset()
{
    last_attack_frame_ = 0;
    attack_ = false;
    upgrade_select_index_ = 0;
	kill_number_ = 0;

    Entity::setMaxHealth(75);
    Entity::setHealth(75);
    Entity::setSpeed(9.9f);
    Entity::setJump(16.5f);

    cl::AnimatedSpriteSheet::loadAnimation("idle");
    lower_body_.loadAnimation("idle");
}

int Player::getKillNumber()
{
	return kill_number_;
}

void Player::incrementKillNumber()
{
	kill_number_++;
}

void Player::resetKillNumber()
{
	kill_number_ = 0;
}

bool Player::isPressingLeft()
{
    return inputs_.left;
}

bool Player::isPressingRight()
{
    return inputs_.right;
}

bool Player::isPressingJump()
{
    return inputs_.jump;
}

void Player::processAnimations()
{
    if (isDisabled())
    {
        if (!isAnimating("idle"))
            cl::AnimatedSpriteSheet::loadAnimation("idle");
        return;
    }

    bool animating_attack = isAnimating("attack");

    if (isAnimating("death"))
    {
		if (!played_death_sound_)
		{
			SoundManager::playSound(SoundManager::GirlDeath);
			played_death_sound_ = true;
		}

    }
    else if (!isAlive())
    {
        cl::AnimatedSpriteSheet::loadAnimation("death");
    }
    else if (isAnimating("attack") && attack_)
    {
        if (getCurrentFrame() == 6 && last_attack_frame_ == 5)
        {
            attack_ = inputs_.shoot;

            int extra_shots = 0;
            float damage_mod = 1.0f;

            if (Upgrades::isUpgraded(Upgrades::MultiShot))
            {
                extra_shots += 2;
                damage_mod *= 0.8f;
            }
            if (Upgrades::isUpgraded(Upgrades::GreaterMultiShot))
            {
                extra_shots += 3;
                damage_mod *= 0.6f;
            }

            if (extra_shots > 0)
            {
                for (int i = -(extra_shots - 1); i < extra_shots; i++)
                {
                    const glm::vec3 offset(getScaling().x > 0.0f ? -0.25f : 0.25f, 0.1f, 0.0f);
                    Projectile *proj = new Arrow();

                    glm::vec3 direction;
                    direction.x = getScaling().x > 0.0f ? 1.0f : -1.0f;
                    direction.y = 0.15f * i;

                    proj->create(getPosition() + offset, direction, true);

                    int extra_damage = 0;
                    if (Upgrades::isUpgraded(Upgrades::Damage))
                        extra_damage += 5;
                    if (Upgrades::isUpgraded(Upgrades::GreaterDamage))
                        extra_damage += 10;

                    int dmg = proj->getDamage() + extra_damage;
                    proj->setDamage(static_cast<int>(dmg * damage_mod));

                    projectiles_.push_back(proj);
                }

                SoundManager::playSound(SoundManager::EnergyArrow, 20);
            }
            else
            {
                const glm::vec3 offset(getScaling().x > 0.0f ? -0.25f : 0.25f, 0.1f, 0.0f);
                Projectile *proj = new Arrow();

                glm::vec3 direction;
                direction.x = getScaling().x > 0.0f ? 1.0f : -1.0f;
                direction.y = 0.0f;

                proj->create(getPosition() + offset, direction, true);

                int extra_damage = 0;
                if (Upgrades::isUpgraded(Upgrades::Damage))
                    extra_damage += 5;
                if (Upgrades::isUpgraded(Upgrades::GreaterDamage))
                    extra_damage += 10;

                int dmg = proj->getDamage() + extra_damage;
                proj->setDamage(static_cast<int>(dmg * damage_mod));

                projectiles_.push_back(proj);
            }

			SoundManager::playSound(SoundManager::EnergyArrow, 20);
        }

        last_attack_frame_ = getCurrentFrame();
    }
    else if (inputs_.shoot && !last_inputs_.shoot)
    {
        cl::AnimatedSpriteSheet::loadAnimation("attack");
        attack_ = true;
    }
    else if (physics_.isJumping() && physics_.getVelocity().y >= 0.0f)
    {
        if (!isAnimating("jump"))
        {
            cl::AnimatedSpriteSheet::loadAnimation("jump");
            // cl::Log(cl::CL_LOG_INFO, "JUMP LOAD");
			SoundManager::playSound(SoundManager::Jump);
        }
    }
    else if (physics_.getVelocity().y < 0.0f)
    {
        if (!isAnimating("fall"))
        {
            cl::AnimatedSpriteSheet::loadAnimation("fall");
            // cl::Log(cl::CL_LOG_INFO, "FALL LOAD");
        }
    }
    else if (isAnimating("walk") && !physics_.isJumping())
    {
        if (glm::abs(physics_.getVelocity().x) < 0.000001f)
        {
            cl::AnimatedSpriteSheet::loadAnimation("idle");
            // cl::Log(cl::CL_LOG_INFO, "IDLE LOAD");
        }
    }
    else if (glm::abs(physics_.getVelocity().x) > 0.0f)
    {
        cl::AnimatedSpriteSheet::loadAnimation("walk");
        // cl::Log(cl::CL_LOG_INFO, "WALK LOAD");
    }
    else
    {
        if (!cl::AnimatedSpriteSheet::isAnimating("idle"))
        {
            cl::AnimatedSpriteSheet::loadAnimation("idle");
            // cl::Log(cl::CL_LOG_INFO, "IDLE LOAD");
        }
    }   

    // LOWER BODY
    if (physics_.isJumping() && physics_.getVelocity().y >= 0.0f)
    {
        if (!lower_body_.isAnimating("jump"))
            lower_body_.loadAnimation("jump");
    }
    else if (physics_.getVelocity().y < 0.0f)
    {
        if (!lower_body_.isAnimating("fall"))
            lower_body_.loadAnimation("fall");
    }
    else if (lower_body_.isAnimating("walk") && !physics_.isJumping())
    {
        if (glm::abs(physics_.getVelocity().x) < 0.000001f)
            lower_body_.loadAnimation("idle");
    }
    else if (glm::abs(physics_.getVelocity().x) > 0.0f)
    {
        lower_body_.loadAnimation("walk");
    }
    else
    {
        if (!lower_body_.isAnimating("idle"))
            lower_body_.loadAnimation("idle");
    }

    glm::vec3 anim_offset = Entity::getAnimOffset();

    if (isAnimating("attack"))
    {
        Entity::setAnimOffset(glm::vec3(getScaling().x > 0.0f ? 0.35f : -0.30f, 0.0f, 0.0f));

        if (!animating_attack)
        {
            cl::AnimatedSpriteSheet::setPosition(getPhysicsPosition() + getHitboxOffset() + getAnimOffset(), true);
            lower_body_.cl::AnimatedSpriteSheet::setPosition(getPhysicsPosition() + getHitboxOffset() + getAnimOffset(), true);
        }
    }
    else if (!isAnimating("attack"))
    {
        Entity::setAnimOffset(glm::vec3(0.0f));

        if (animating_attack)
        {
            cl::AnimatedSpriteSheet::setPosition(getPhysicsPosition() + getHitboxOffset() + getAnimOffset(), true);
            lower_body_.cl::AnimatedSpriteSheet::setPosition(getPhysicsPosition() + getHitboxOffset() + getAnimOffset(), true);
        }
    }
}

void Player::setAttackSpeed(float time)
{
    cl::AnimatedSpriteSheet::setAnimationTiming("attack", time);
}
