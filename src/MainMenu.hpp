#pragma once
#include <Clone/Core.hpp>
#include <Clone/Graphics.hpp>
#include <Clone/2D.hpp>

class MainMenu
{
public:
    MainMenu();

    void initialize(cl::AssetCache *cache);
    void render(cl::Renderer *renderer);
    void update(float dt);

    bool inMenu() const { return in_menu_; }

private:
    cl::Image main_screen_;
    cl::Image help_screen_;
    bool in_menu_;
    bool start_held_;

    enum MenuState
    {
        Main,
        Help
    };

    MenuState state_;

};