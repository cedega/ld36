#include "Bat.hpp"

void Bat::initialize(cl::AssetCache* cache)
{
	cl::AssetLoader loader;
	cl::AssetLoader::TextureArguments texargs;
	texargs.loader_args = cl::AssetLoader::IR;
	texargs.min_filter = cl::Texture::Linear;
	texargs.mag_filter = cl::Texture::Linear;
	cl::Texture *texture = loader.loadTexture("data/bat/bat.png", texargs, cache);
	cl::AnimatedSpriteSheet::initialize(texture, ShaderManager::shader2d, false, glm::ivec2(100, 100), 2);

	cl::AnimatedSpriteSheet::createAnimation("run", 1, 4, 50 / 1000.0f);
	cl::AnimatedSpriteSheet::createAnimation("hit", 5, 5, 50 / 1000.0f);
	cl::AnimatedSpriteSheet::createAnimation("death", 6, 10, 50 / 1000.0f);

	cl::AnimatedSpriteSheet::setLooping("hit", false);
	cl::AnimatedSpriteSheet::setLooping("death", false);

	cl::AnimatedSpriteSheet::loadAnimation("run");

	Entity::setHitboxOffset(glm::vec3(0.25f, 0.85f, 0.25f));
	hitbox_.radius = glm::vec3(0.6f, 0.6, 0.3f);
	cl::AnimatedSpriteSheet::setScaling(glm::vec3(0.03f), true);

	// strats
	//PatrolStrategy p(glm::vec3(1.0f), glm::vec3(5.0f));
    setHealth(static_cast<int>(20 * scaling_));
	setSpeed(2.5f);
	setJump(2.5f);
	setDamage(scaling_ * 4.0f); 
}

void Bat::update(float dt)
{ 
	// bats are gay and don't use the parent functions
    cl::AnimatedSpriteSheet::updateLastState();
    hurt_tick_ += dt;

	glm::vec3 pPos = player_.getPosition();
	// the entire reason we can't use Enemy::update(dt) is because we don't want bats to be affected by gravity
    glm::vec3 velocity;

    if (!isAlive())
    {
        velocity.x = 0.0f;
        velocity.y = physics_.getVelocity().y;
    }
    else
    {
        velocity.y = pPos.y > getPosition().y ? getJump() : -1 * getJump();
        velocity.x = pPos.x > getPosition().x ? getSpeed() : -1 * getSpeed();
    }

	physics_.setVelocity(velocity);
	physics_.integrate(dt);
	hitbox_.centre = physics_.getPosition(); 
	// ... so we have to replace everything else
    for (unsigned int i = 0; i < collisions_->getOBBs().size(); i++)
    {
        glm::vec3 mtv;
        if (cl::intersect3DSAT(hitbox_, collisions_->getOBBs()[i], &mtv))
        {
            hitbox_.centre += mtv;

            if (glm::dot(glm::vec3(0.0f, 1.0f, 0.0f), glm::normalize(mtv)) > 0.5f)
            {
                if (physics_.getVelocity().y < 0.1f)
                {
                    physics_.setVelocity(physics_.getVelocity() * glm::vec3(1.0f, 0.0f, 1.0f));
                    physics_.land();
                }

            }
            else if (glm::dot(glm::vec3(0.0f, -1.0f, 0.0f), glm::normalize(mtv)) > 0.5f)
            {
                if (physics_.getVelocity().y > 0.1f)
                {
                    physics_.setVelocity(physics_.getVelocity() * glm::vec3(1.0f, 0.0f, 1.0f));
                }
            }
        }
    }

	// dank collision memes
	damageTick_ += dt;
	if (!Entity::isDisabled() && damageTick_ > 1.0 && isAlive() && cl::intersect3DSAT(hitbox_, player_.getHitbox(), NULL))
	{
		SoundManager::playSound(SoundManager::Bat, 40);
		player_.damage(static_cast<int>(damage_));
		damageTick_ = 0.0f;
	}
    
	physics_.setPosition(hitbox_.centre);
	cl::AnimatedSpriteSheet::setPosition(hitbox_.centre, false);
	processAnimations();
	cl::AnimatedSpriteSheet::update(dt);
}

void Bat::processAnimations()
{
	Enemy::processAnimations();

	if (isAnimating("hit") && sound_is_not_playing_)
	{
		SoundManager::playSound(SoundManager::EnemyHit);
		sound_is_not_playing_ = false;
	}
	else if (isAnimating("run"))
	{
		sound_is_not_playing_ = true;
	}
	else if (isAnimating("death") && !death_sound_played_)
	{
		SoundManager::playSound(SoundManager::BatDeath);
		SoundManager::playSound(SoundManager::EnemyHit);
		death_sound_played_ = true;
	}
	else
		sound_is_not_playing_ = false;
}

