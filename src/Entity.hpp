#pragma once
#include <Clone/2D/AnimatedSpriteSheet.hpp>
#include <Clone/Physics/EulerPhysics.hpp>
#include <Clone/Geometry.hpp>
#include <Clone/Core.hpp>
#include "Collisions.hpp"
#include "Projectile.hpp"

class Entity : public cl::AnimatedSpriteSheet
{
public:
    Entity();
    virtual ~Entity();

    void update(float dt);
    void setCollisions(Collisions *collisions);
    void setPosition(const glm::vec3 &pos);
    glm::vec3 getPhysicsPosition() const;
    
    void setSpeed(float speed);
    float getSpeed() const;

    void setJump(float jump);
    float getJump() const;

    void setHitboxOffset(const glm::vec3 &offset);
    const glm::vec3& getHitboxOffset() const;

    void setAnimOffset(const glm::vec3 &offset);
    const glm::vec3& getAnimOffset() const;

    cl::EulerPhysics& getPhysics();
    const cl::OBB3D& getHitbox() const;

    void setHealth(int health);
    int getHealth() const;

    void setMaxHealth(int max_health);
    int getMaxHealth() const;

    bool isAlive();
    void damage(int damage);

	virtual void onDeath() {}

    static void setDisabled(bool b);
    static bool isDisabled();

    std::vector<Projectile*>& getProjectiles() { return projectiles_; }

protected:
    static bool disabled_;
    Collisions *collisions_;
    cl::EulerPhysics physics_;
    cl::OBB3D hitbox_;
    glm::vec3 hitbox_offset_;
    glm::vec3 anim_offset_;
    std::vector<Projectile*> projectiles_;

    float speed_;
    float jump_height_;
    int max_health_;
    int health_;
    float hurt_tick_;
    bool is_stunnable_;

    virtual bool isPressingLeft() = 0;
    virtual bool isPressingRight() = 0;
    virtual bool isPressingJump() = 0;
    virtual void processAnimations() = 0;
};