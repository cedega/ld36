#include "Boss.hpp"
#include "ShaderManager.hpp"

void Boss::initialize(cl::AssetCache* cache)
{
	cl::AssetLoader loader;
	cl::AssetLoader::TextureArguments texargs;
	texargs.loader_args = cl::AssetLoader::IR;
	texargs.min_filter = cl::Texture::Linear;
	texargs.mag_filter = cl::Texture::Linear;
	cl::Texture *texture = loader.loadTexture("data/boss/Bossfinal.png", texargs, cache);
	cl::AnimatedSpriteSheet::initialize(texture, ShaderManager::shader2d, false, glm::ivec2(300, 300), 2);

	cl::AnimatedSpriteSheet::createAnimation("idle", 1, 9, 75 / 1000.0f);
	cl::AnimatedSpriteSheet::createAnimation("run", 10, 17, 50 / 1000.0f);
	cl::AnimatedSpriteSheet::createAnimation("attack", 18, 25, 50 / 1000.0f);
	cl::AnimatedSpriteSheet::createAnimation("death", 27, 35, 50 / 1000.0f);
	cl::AnimatedSpriteSheet::createAnimation("jump", 26, 26, 50 / 1000.0f);

	cl::AnimatedSpriteSheet::setLooping("death", false);
	cl::AnimatedSpriteSheet::setLooping("jump", false);
	cl::AnimatedSpriteSheet::setLooping("attack", false);

	cl::AnimatedSpriteSheet::loadAnimation("run");

	Entity::setHitboxOffset(glm::vec3(0.0f, 3.5f, 0.25f));
	hitbox_.radius = glm::vec3(1.5f, 2.5f, 0.3f);
	cl::AnimatedSpriteSheet::setScaling(glm::vec3(0.04f), true);

    cl::AnimatedSpriteSheet::setFrameTiming("attack", 18, 650.0f / 1000.0f);
    cl::AnimatedSpriteSheet::setFrameTiming("attack", 25, 450.0f / 1000.0f);

	// strats
	//PatrolStrategy p(glm::vec3(1.0f), glm::vec3(5.0f));
    setHealth(static_cast<int>(2000 * scaling_));
	setSpeed(3.0f);
	setJump(18.5f);
	setDamage(scaling_ * 12.5f);
    attack_damage_ = scaling_ * 40.0f;
    is_stunnable_ = false;
}

void Boss::update(float dt)
{
	if (getStrategy(state::STRAT_CHASE))
	{
		setStrategy(state::STRAT_CHASE);
	}
	Enemy::update(dt);
}

void Boss::processAnimations()
{
	if (isAnimating("hit") && sound_is_not_playing_)
	{
		SoundManager::playSound(SoundManager::BossHurt, 128);
		SoundManager::playSound(SoundManager::EnemyHit);
		sound_is_not_playing_ = false;
	}
	else if (isAnimating("run"))
	{
		sound_is_not_playing_ = true;
	}
	else if (isAnimating("death") && !death_sound_played_)
	{
		SoundManager::playSound(SoundManager::BossDie, 50);
		death_sound_played_ = true;
	}
	else if (isAnimating("attack") && sound_is_not_playing_)
	{
		SoundManager::playSound(SoundManager::BossAttack);
		sound_is_not_playing_ = false;
	}
	else
		sound_is_not_playing_ = false;

}

bool Boss::getAttackBox(cl::OBB3D *output)
{
    if (!isAnimating("attack"))
        return false;

    unsigned int frame = getCurrentFrame();

    if (frame == 6)
    {
        glm::vec3 pos = getPosition() + glm::vec3(0.0f, 1.0f, 0.0f);
        glm::vec3 rad(5.0f, 3.0f, 3.0f);
        *output = cl::OBB3D(pos, rad);
        return true;
    }
    else if (frame == 7)
    {
        glm::vec3 pos = getPosition() + glm::vec3(getScaling().x > 0.0f ? 3.5f : -3.5f, -1.0f, 0.0f);
        glm::vec3 rad(3.0f, 5.0f, 3.0f);
        *output = cl::OBB3D(pos, rad);
        return true;
    }

    return false;
}

void Boss::drawAttackDebug(cl::Renderer *renderer, cl::DebugRenderObject *debug)
{
    if (!isAnimating("attack"))
        return;

    cl::OBB3D obb;
    if (getAttackBox(&obb))
    {
        debug->setShape3D(obb, ShaderManager::debug);
        renderer->draw(debug);       
    }
}