#pragma once
#include "EnemyBehaviourStrategy.hpp"
#include <Clone/Core.hpp>

class PatrolStrategy : public EnemyBehaviourStrategy
{
public:
	PatrolStrategy(const glm::vec3 pt1, const glm::vec3 pt2);

	void move(Enemy& e, Player& p);
	void die();
	void attack();
private:
	glm::vec3 point1_, point2_;
	glm::vec3 currentDestination_;
};