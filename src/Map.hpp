#pragma once
#include <Clone/Graphics.hpp>
#include "Level.hpp"

class Map
{
friend class Engine;
public:
    Map();
    ~Map();

    enum TransitionState
    {
        None = 0,
        FadingOut,
        FadingIn
    };

    void initialize();
    void loadLevel(Level::Enum level);
    void update(float dt);
    void render(cl::Renderer *renderer);
    void renderFade(cl::Renderer *renderer);

    bool hasWon();
    void setEnemyScaling(Level::Enum e);

private:
    static Player *player_;
    Level *current_level_;
    TransitionState tran_state_;
    float fade_tick_;
    cl::Texture *black_;
    cl::Image fade_image_;
    cl::Camera fade_camera_;
    bool reseting_;
    bool final_level_;
};