#include "EnemyManager.hpp"

std::vector<Enemy*> EnemyManager::enemies_;
EnemyManager* EnemyManager::instance_;

Enemy* EnemyManager::addEnemy(Enemy* e)
{
	if (instance_ == NULL)
		instance_ = new EnemyManager();

	instance_->enemies_.push_back(e);
	return e;
}

void EnemyManager::clear()
{
	for (std::vector<Enemy*>::const_iterator itr = enemies_.begin(); itr != enemies_.end(); ++itr)
	{
		delete *itr;
	}
	enemies_.clear();
}

std::vector<Enemy*>& EnemyManager::getEnemies()
{ 
	return enemies_;
}

EnemyManager::EnemyManager()
{
	// do noothing
}

EnemyManager::~EnemyManager()
{
	EnemyManager::clear(); 
}
void EnemyManager::initEnemies(cl::AssetCache* cache)
{
	for (std::vector<Enemy*>::iterator enemy = EnemyManager::getEnemies().begin(); enemy != EnemyManager::getEnemies().end(); ++enemy)
	{
		(*enemy)->initialize(cache);
	}
}