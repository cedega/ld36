#include <Clone/Core.hpp>
#include "ShaderManager.hpp"
#include "Player.hpp"
#include "EnemyManager.hpp"
#include "UI.hpp"

UI::UI()
{

}

void UI::initialize(cl::AssetCache *cache)
{
    cl::AssetLoader loader;
    cl::AssetLoader::TextureArguments texargs;
    cl::Texture *hp = loader.loadTexture("data/ui/Green.png", texargs, cache);
    cl::Texture *hp_full = loader.loadTexture("data/ui/Empty.png", texargs, cache);
	cl::Texture *key = loader.loadTexture("data/ui/emptyskeletonkey.png", texargs, cache);
	cl::Texture *key_full = loader.loadTexture("data/ui/fullskeletonkey.png", texargs, cache);

    health_.initialize(hp, ShaderManager::shader2d, true);
    health_max_.initialize(hp_full, ShaderManager::shader2d, true);
	key_.initialize(key, ShaderManager::shader2d, true);
	key_max_.initialize(key_full, ShaderManager::shader2d, true);

    health_.setScaling(glm::vec3(0.25f), true);
    health_max_.setScaling(glm::vec3(0.25f), true);

	key_.setScaling(glm::vec3(0.25f), true);
	key_max_.setScaling(glm::vec3(0.25f), true);

    health_.setTextureRect(glm::vec4(0, 0, 1525, 187));
    health_max_.setPosition(glm::vec3(0.0f), cl::Image::TopLeft, true);
    health_.setPosition(glm::vec3(420, 151, 0) * health_.getScaling(), cl::Image::TopLeft, true);

	key_.setTextureRect(glm::vec4(0, 0, 2048, 1024));
	key_max_.setPosition(glm::vec3(0.0f), cl::Image::TopRight, true);
	key_.setPosition(glm::vec3(5500, 100, 0) * key_.getScaling(), cl::Image::TopLeft, true);
}

void UI::render(cl::Renderer *renderer)
{
    renderer->draw(&health_);
    renderer->draw(&health_max_);
	renderer->draw(&key_);
	renderer->draw(&key_max_);
}

void UI::update(Player *player, float dt)
{
    float ratio = player->getHealth() / static_cast<float>(player->getMaxHealth());
    health_.setTextureRect(glm::vec4(0, 0, 1525 * ratio, 187));
    health_.setPosition(glm::vec3(420, 151, 0) * health_.getScaling(), cl::Image::TopLeft, true);

	unsigned int enemies = static_cast<unsigned int>(floor(EnemyManager::getEnemies().size() * 0.7f));
	float key_ratio;

	if (enemies > 0)
	{
		key_ratio = player->getKillNumber() / static_cast<float>(enemies);

		if (key_ratio > 1.0f)
			key_ratio = 1.0f;
	}
	else
	{
		key_ratio = 1.0f;
	}

	key_max_.setTextureRect(glm::vec4(0, 0, 2048 * key_ratio, 1024));
	key_max_.setPosition(glm::vec3(5500, 100, 0) * key_.getScaling(), cl::Image::TopLeft, true);
}
