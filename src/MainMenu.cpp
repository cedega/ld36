#include "MainMenu.hpp"
#include "ShaderManager.hpp"

MainMenu::MainMenu() :
    in_menu_(true),
    start_held_(false),
    state_(Main)
{

}

void MainMenu::initialize(cl::AssetCache *cache)
{
    cl::AssetLoader loader;
    cl::AssetLoader::TextureArguments texargs;
    texargs.min_filter = cl::Texture::Linear;
    texargs.mag_filter = cl::Texture::Linear;

    cl::Texture *tex = loader.loadTexture("data/mainmenu/main.png", texargs, cache);
    main_screen_.initialize(tex, ShaderManager::shader2d, true);
    main_screen_.setTextureRect(glm::vec4(0, 0, 1920, 1080));
    main_screen_.setPosition(glm::vec3(0.0f), cl::Image::TopLeft, true);

    cl::Texture *tex2 = loader.loadTexture("data/mainmenu/help.png", texargs, cache);
    help_screen_.initialize(tex2, ShaderManager::shader2d, true);
    help_screen_.setTextureRect(glm::vec4(0, 0, 1920, 1080));
    help_screen_.setPosition(glm::vec3(0.0f), cl::Image::TopLeft, true);
}

void MainMenu::render(cl::Renderer *renderer)
{
    if (state_ == Main)
        renderer->draw(&main_screen_);
    else
        renderer->draw(&help_screen_);
}

void MainMenu::update(float dt)
{
    cl::Keys keys = cl::Input::getKeyboardState();
    bool pressed = keys[CL_KEY_Z] >= 1;

    if (!cl::Input::getControllers()->empty())
    {
        SDL_GameController *pad = cl::Input::getControllers()->at(0);
        pressed |= cl::Input::getControllerButton(pad, SDL_CONTROLLER_BUTTON_X);
    }

    if (pressed && !start_held_)
    {
        if (state_ == Main)
            state_ = Help;
        else
            in_menu_ = false;
    }

    start_held_ = pressed;
}