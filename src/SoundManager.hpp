#pragma once
#include <Clone/Core.hpp>
#include <Clone/Audio.hpp>

class SoundManager
{
public:
	enum SoundEffect
	{
		EnergyArrow,
		Jump,
		Bark,
		Growl,
		Break,
		Bat,
		EnemyHit,
		DogDeath,
		BatDeath,
		GirlDeath,
		GirlHurt,
		GuyDeath,
		GuyHurt,
		BossAttack,
		BossImpact,
		BossHurt,
		BossLaugh,
		BossDie,
		BossStep,
		Proceed,
		Upgrade,
		Select
	};

	enum MusicEffect
	{
		Sand,
		Tech,
		Boss
	};

	static void initialize(cl::AssetCache *cache);

	static void playSound(SoundEffect se, int volume=50);
	static void playMusic(MusicEffect me, int volume=50);
	static void stopMusic();

private:
	SoundManager() {}
	static cl::AssetCache *cache_;
	static std::string swizzleSoundEnum(SoundEffect se);
	static std::string swizzleMusicEnum(MusicEffect me);
};