#pragma once
#include "Enemy.hpp"

class Oblisk : public Enemy
{ 
public:
	Oblisk(Player& p) : Enemy(p) {};
	Oblisk(glm::vec3 pos, Player& p) : Enemy(p) { setPosition(pos); };

	void initialize(cl::AssetCache* cache);
	void update(float dt);
	void processAnimations();
private:
	float attack_timer_ = 0.5f;
	float attack_tick_ = 0.0f;
};
