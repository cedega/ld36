#include "Spearman.hpp"

void Spearman::initialize(cl::AssetCache* cache)
{
	cl::AssetLoader loader;
	cl::AssetLoader::TextureArguments texargs;
	texargs.loader_args = cl::AssetLoader::IR;
	texargs.min_filter = cl::Texture::Linear;
	texargs.mag_filter = cl::Texture::Linear;
	cl::Texture *texture = loader.loadTexture("data/basic_melee/basicmelee.png", texargs, cache);
	cl::AnimatedSpriteSheet::initialize(texture, ShaderManager::shader2d, false, glm::ivec2(200, 200), 2);

	cl::AnimatedSpriteSheet::createAnimation("run", 1, 10, 50 / 1000.0f);
	cl::AnimatedSpriteSheet::createAnimation("hit", 11, 11, 50 / 1000.0f);
	cl::AnimatedSpriteSheet::createAnimation("death", 12, 16, 50 / 1000.0f);
	cl::AnimatedSpriteSheet::createAnimation("jump", 10, 10, 50 / 1000.0f);

	cl::AnimatedSpriteSheet::setLooping("hit", false);
	cl::AnimatedSpriteSheet::setLooping("death", false);
	cl::AnimatedSpriteSheet::setLooping("jump", false);

	cl::AnimatedSpriteSheet::loadAnimation("run");

	Entity::setHitboxOffset(glm::vec3(0.0f, 1.0f, 0.25f));
	hitbox_.radius = glm::vec3(0.7f, 1.25f, 0.1f);
	cl::AnimatedSpriteSheet::setScaling(glm::vec3(0.03f * 0.75f), true);

	// strats
	//PatrolStrategy p(glm::vec3(1.0f), glm::vec3(5.0f));
    setHealth(static_cast<int>(65 * scaling_));
	setSpeed(3.0f);
	setJump(10.5f);
	setDamage(scaling_ * 8.0f);
}

void Spearman::update(float dt)
{
	if (getStrategy(state::STRAT_CHASE))
	{
		setStrategy(state::STRAT_CHASE);
	}
	Enemy::update(dt);
}

void Spearman::processAnimations()
{
	Enemy::processAnimations();

	if (isAnimating("hit") && sound_is_not_playing_)
	{
		SoundManager::playSound(SoundManager::GuyHurt, 50);
		SoundManager::playSound(SoundManager::EnemyHit);
		sound_is_not_playing_ = false;
	}
	else if (isAnimating("run"))
	{
		sound_is_not_playing_ = true;
	}
	else if (isAnimating("death") && !death_sound_played_)
	{
		SoundManager::playSound(SoundManager::GuyDeath, 40);
		death_sound_played_ = true;
	}
	else
		sound_is_not_playing_ = false;

}