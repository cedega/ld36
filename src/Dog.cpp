#include "Dog.hpp"
#include <iostream>

void Dog::initialize(cl::AssetCache* cache)
{
	cl::AssetLoader loader;
	cl::AssetLoader::TextureArguments texargs;
	texargs.loader_args = cl::AssetLoader::IR;
	texargs.min_filter = cl::Texture::Linear;
	texargs.mag_filter = cl::Texture::Linear;
	cl::Texture *texture = loader.loadTexture("data/dog/dog.png", texargs, cache);
	cl::AnimatedSpriteSheet::initialize(texture, ShaderManager::shader2d, false, glm::ivec2(128, 128), 2);

	cl::AnimatedSpriteSheet::createAnimation("run", 1, 5, 50 / 1000.0f);
	cl::AnimatedSpriteSheet::createAnimation("hit", 6, 6, 50 / 1000.0f);
	cl::AnimatedSpriteSheet::createAnimation("death", 7, 11, 50 / 1000.0f);
	cl::AnimatedSpriteSheet::createAnimation("jump", 5, 5, 50 / 1000.0f);

	cl::AnimatedSpriteSheet::setLooping("hit", false);
	cl::AnimatedSpriteSheet::setLooping("death", false);
	cl::AnimatedSpriteSheet::setLooping("jump", false);

	cl::AnimatedSpriteSheet::loadAnimation("run");

	Entity::setHitboxOffset(glm::vec3(0.0f, 1.2f, 0.0f));
	hitbox_.radius = glm::vec3(0.8f, 0.75f, 0.3f);
	cl::AnimatedSpriteSheet::setScaling(glm::vec3(0.03f), true);

	// strats
	//PatrolStrategy p(glm::vec3(1.0f), glm::vec3(5.0f));
    setHealth(static_cast<int>(25 * scaling_));
	setSpeed(5.0f);
	setJump(6.5f);
	setDamage(scaling_ * 5.5f);
}

void Dog::update(float dt)
{
	if (getStrategy(state::STRAT_CHASE))
	{
		setStrategy(state::STRAT_CHASE);
	}
	Enemy::update(dt);
}

void Dog::processAnimations()
{
	Enemy::processAnimations();

	if (isAnimating("hit") && sound_is_not_playing_)
	{
		SoundManager::playSound(SoundManager::Bark, 100);
		SoundManager::playSound(SoundManager::EnemyHit);
		sound_is_not_playing_ = false;
	}
	else if (isAnimating("run"))
	{
		sound_is_not_playing_ = true;
	}
	else if (isAnimating("death") && !death_sound_played_)
	{
		SoundManager::playSound(SoundManager::DogDeath, 75);
		SoundManager::playSound(SoundManager::EnemyHit);
		death_sound_played_ = true;
	}
	else
		sound_is_not_playing_ = false;
}

void Dog::playAttackSound()
{
	SoundManager::playSound(SoundManager::Growl, 100);
}