#pragma once
#include <Clone/Core.hpp>
// a node for enemies to use to track movement
class Node
{
public:
	static glm::vec3 getNearestNode(const glm::vec3 enemyPos, const glm::vec3 playerPos); // returns the position of the nearest node
	static void initNodes(const int level);
private:
	static std::vector<Node*> *mNodes;
	Node(glm::vec3 pos);
	~Node();
	glm::vec3 mPos;
};
