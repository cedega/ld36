#pragma once
#include <Clone/Graphics.hpp>
#include "Player.hpp"
#include "Tile.hpp"
#include "Collisions.hpp"
#include "EnemyManager.hpp"
#include "PatrolStrategy.hpp"
#include "ChaseStrategy.hpp"
#include "Upgrades.hpp"

class Level
{
friend class Engine;
friend class Map;
public:
    Level();
    virtual ~Level();

    enum Enum
    {
        Stay = 0,
        Debug,
        LevelOne,
		LevelTwo,
		LevelUpgrade,
		LevelThree,
		LevelFour,
		LevelStart,
		LevelFive
    };

    void addUpgradeModel(int tile_x, int tile_y, float x, float y, float z);
    void addProp(const std::string &file_name, int tile_x, int tile_y, float x, float y, float z);
    void addSpikes(int tile_x, int tile_y, float x, float y, float width, float height);
    Enemy* addEnemy(Enemy *enemy, int tile_x, int tile_y, float x, float y);
    Enemy* addBoss(Enemy *enemy, int tile_x, int tile_y, float x, float y);
    void addTile(const std::string &file_name, int x, int y);
    void mergeCollisions();
    void update(float dt);
    void render(cl::Renderer *renderer);
	bool canProgress();

    Enum getNextArea() const { return exiting_ ? next_area_ : Level::Stay; }
    glm::vec3 getTilePosition(int x, int y) { return glm::vec3(16.0f * x, 9.0f * y, 2.0f); }

protected:
    static Enum PreviousLevel;
    static Player *player_;
    static cl::AssetCache *cache_;
    static cl::Camera *camera_;
    cl::DebugRenderObject debug_;
    std::vector<Tile*> tiles_;
    std::vector<cl::Model> props_;
    std::vector<cl::OBB3D> spikes_;
    bool upgrade_present_;
    cl::Model upgrade_model_;
    cl::OBB3D upgrade_area_;
    Collisions collisions_;
    cl::OBB3D exit_area_;
    Enum next_area_;
    bool exiting_;
    Upgrades::Power upgrades_[3];
    bool show_upgrades_;
    bool upgraded_;
	bool played_key_sound_;
};