#pragma once
#include "../Level.hpp"
#include "../TestEnemy.hpp"
#include "../Dog.hpp"
#include "../Bat.hpp"
#include "../Oblisk.hpp"

class LevelDebug : public Level
{
public:
    LevelDebug()
    {
        const std::string BLANK_TILE = "Level_0/00_0000_0000_0000_1.cm";
        const std::string RIGHT_WALL_FLOOR = "Level_0/01_0010_0000_0000_0.cm";
        const std::string PLATFORM_LEFT_WALL = "Level_0/10_0001_0000_0000_0.cm";
        const std::string LEFT_STAIRCASE = "Level_0/10_0010_0001_1000_0.cm";

        // Load necessary tiles
        addTile(BLANK_TILE, 0, 0);
        addTile(BLANK_TILE, 1, 0);
        addTile(BLANK_TILE, 2, 0);
        addTile(BLANK_TILE, 3, 0);

        addTile(BLANK_TILE, 0, 1);
        addTile(LEFT_STAIRCASE, 1, 1);
        addTile(RIGHT_WALL_FLOOR, 2, 1);
        addTile(BLANK_TILE, 3, 1);

        addTile(BLANK_TILE, 0, 2);
        addTile(PLATFORM_LEFT_WALL, 1, 2);
        addTile(RIGHT_WALL_FLOOR, 2, 2);
        addTile(BLANK_TILE, 3, 2);

        addTile(BLANK_TILE, 0, 3);
        addTile(BLANK_TILE, 1, 3);
        addTile(BLANK_TILE, 2, 3);
        addTile(BLANK_TILE, 3, 3);

        player_->setPosition(getTilePosition(2, 2) + glm::vec3(0.0f, 0.0f, 0.0f));

		// Don't change
        camera_->snapTo(player_->getPhysicsPosition());
        camera_->moveTo(player_->getPhysicsPosition(), 1.0f);

		// add enemy
        /*Enemy *e = addEnemy(new TestEnemy(*player_), 0, 0, 32.0f, 18.0f);
		e->addStrategy(new PatrolStrategy(glm::vec3(1.0f, 10.0f, 0.0f), glm::vec3(-4.0f, 10.0f, 0.0f)));
        e->setStrategy(state::STRAT_PATROL);
		*/

        //addEnemy(new TestEnemy(*player_), 0, 0, 32.0f, 30.0f);
        addEnemy(new Dog(*player_), 0, 0, 32.0f, 30.0f);

		addEnemy(new Bat(*player_), 0, 0, 32.0f, 30.0f);

		addEnemy(new Oblisk(*player_), 0, 0, 32.0f, 30.0f);
		// make sure these two lines are called; will be obvious if not since everything will crash
		EnemyManager::initEnemies(cache_);
        mergeCollisions();

        exit_area_.centre = getTilePosition(2, 3);
        exit_area_.centre.x += 0.0f;
        exit_area_.centre.y += 0.0f;
        exit_area_.radius = glm::vec3(1.0f);

        next_area_ = Level::LevelOne;
    }

private:

};