#pragma once
#include "../Level.hpp"
#include "../TestEnemy.hpp"
#include "../Dog.hpp"
#include "../Spearman.hpp"
#include "../Ranged.hpp"

class LevelFour : public Level
{
public:
	LevelFour()
	{
		const std::string BLANK_TILE = "Level_1/00_0000_0000_0000_0.cm";
		const std::string BG = "Level_1/00_0000_0000_0000_1.cm";
		const std::string AAAAA = "Level_4/aaaaa.cm";
		const std::string BBBBB = "Level_4/bbbbb.cm";
		const std::string CCCCC = "Level_4/ccccc.cm";
		const std::string DDDDD = "Level_4/ddddd.cm";
		const std::string EEEEE = "Level_4/eeeee.cm";
		const std::string FFFFF = "Level_4/fffff.cm";
		const std::string GGGGG = "Level_4/ggggg.cm";
		const std::string HHHHH = "Level_4/hhhhh.cm";


		addProp("door.cm", 1, 2, 0.0f, 1.0f, 2.0f);
		addProp("door.cm", 5, 2, 0.0f, 1.0f, 2.0f);

		addSpikes(1, 1, 0.0f, 0.0f, 6.0f, 3.0f);
		addSpikes(2, 1, 0.0f, 0.0f, 6.0f, 3.0f);
		addSpikes(3, 1, 0.0f, 0.0f, 6.0f, 3.0f);
		addSpikes(4, 1, 0.0f, 0.0f, 6.0f, 3.0f);


		addTile(BG, 0, 0);
		addTile(BG, 1, 0);
		addTile(BG, 2, 0);
		addTile(BG, 3, 0);
		addTile(BG, 4, 0);
		addTile(BG, 6, 1);
		addTile(BG, 4, -1);
		addTile(BG, 6, 0);
		addTile(BG, 5, 0);


		addTile(BG, 0, 1);
		addTile(BG, 0, 2);
		addTile(BG, 0, 3);
		addTile(BG, 1, 1);

		addTile(BG, 1, 3);
		addTile(BG, 2, 3);
		addTile(BG, 3, 3);
		addTile(BG, 4, 3);
		addTile(BG, 5, 3);

		addTile(BG, 6, 2);
		addTile(BG, 6, 3);

		addTile(BLANK_TILE, 4, 2);

		addTile(AAAAA, 1, 2);
		addTile(BBBBB, 2, 2);
		addTile(CCCCC, 3, 2);
		addTile(EEEEE, 5, 2);
		addTile(FFFFF, 2, 1);
		addTile(GGGGG, 3, 1);
		addTile(HHHHH, 4, 1);
		addTile(BG, 5, 1);




		player_->setPosition(getTilePosition(1 , 2) + glm::vec3(0.0f, 0.0f, 0.0f));
		camera_->snapTo(player_->getPhysicsPosition());
		camera_->moveTo(player_->getPhysicsPosition(), 1.0f);

		// Add Enemies

		
		addEnemy(new Dog(*player_), 1, 2, -7.0f, 0.0f);
		addEnemy(new Bat(*player_), 1, 1, 5.0f, 0.0f);
		addEnemy(new Bat(*player_), 1, 1, 6.0f, 0.0f);
		addEnemy(new Bat(*player_), 2, 1, 5.0f, 0.0f);
		addEnemy(new Bat(*player_), 2, 1, 5.0f, 0.0f);
		addEnemy(new Bat(*player_), 3, 1, 5.0f, 0.0f);
		addEnemy(new Bat(*player_), 4, 1, 5.0f, 0.0f);
		addEnemy(new Bat(*player_), 3, 0, 5.0f, 0.0f);
		addEnemy(new Bat(*player_), 4, 1, 10.0f, 0.0f);
		addEnemy(new Bat(*player_), 1, 2, 5.0f, 0.0f);
		addEnemy(new Bat(*player_), 2, 1, 5.0f, 10.0f);
		addEnemy(new Bat(*player_), 2, 1, 5.0f, 7.0f);
		addEnemy(new Oblisk(*player_), 2, 1, 11.0f, 10.0f);
		addEnemy(new Oblisk(*player_), 2, 1, 0.0f, 11.0f);
		addEnemy(new Oblisk(*player_), 3, 1, 5.0f, 11.0f);
		addEnemy(new Oblisk(*player_), 3, 1, 3.0f, 11.0f);
		addEnemy(new Oblisk(*player_), 3, 1, 15.0f, 11.0f);
		addEnemy(new Spearman(*player_), 4, 1, 15.0f, 10.0f);

		// make sure these two lines are called; will be obvious if not since everything will crash
		EnemyManager::initEnemies(cache_);
		mergeCollisions();

		exit_area_.centre = getTilePosition(5, 2);
		exit_area_.centre.x += 0.0f;
		exit_area_.centre.y += 3.0f;
		exit_area_.radius = glm::vec3(1.0f);

		next_area_ = Level::LevelUpgrade;
	}

private:

};
