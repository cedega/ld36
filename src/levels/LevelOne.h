#pragma once
#include "../Level.hpp"
#include "TestEnemy.hpp"

#define BLANK_TILE "Level_0/00_0000_0000_0000_1.cm"
#define RIGHT_WALL_FLOOR "Level_0/01_0010_0000_0000_0.cm"
#define PLATFORM_LEFT_WALL "Level_0/10_0001_0000_0000_0.cm"
#define LEFT_STAIRCASE "Level_0/10_0010_0001_1000_0.cm"

class LevelOne : public Level
{
public:
	LevelOne()
	{
		addTile(BLANK_TILE, 0, 0);
		addTile(BLANK_TILE, 1, 0);
		addTile(BLANK_TILE, 2, 0);
		addTile(BLANK_TILE, 3, 0);
		addTile(BLANK_TILE, 3, 1);
		addTile(BLANK_TILE, 2, 1);

		player_->setPosition(glm::vec3(16.0f * 2, 9.0f * 2, 2.0f));
		camera_->snapTo(player_->getPosition());

		mergeCollisions();

		exit_area_.centre = glm::vec3(5.0f, 2.5f, 0.0f);
		exit_area_.radius = glm::vec3(3.0f);
		next_area_ = Level::Debug;
	}
}