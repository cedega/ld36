#pragma once
#include "../Level.hpp"
#include "../TestEnemy.hpp"
#include "../Dog.hpp"
#include "../Spearman.hpp"
#include "../Ranged.hpp"
#include "../Boss.hpp"

class LevelFive: public Level
{
public:
	LevelFive()
	{
		SoundManager::playSound(SoundManager::BossLaugh, 75);
		SoundManager::stopMusic();
		SoundManager::playMusic(SoundManager::Boss, 15);

		const std::string BG = "Level_1/00_0000_0000_0000_1.cm";
		const std::string AAAA = "Boss_Room/aaaa.cm";
		const std::string AAAAA = "Level_4/aaaaa.cm";
		const std::string BBBB = "Boss_Room/bbbb.cm";
		const std::string CCCC = "Boss_Room/cccc.cm";
		const std::string DDDD = "Boss_Room/dddd.cm";
		const std::string JJ = "Level_3/jj.cm";
		const std::string CC = "Level_3/cc.cm";
		const std::string BASIC_1 = "Level_1/10_0010_1000_0001_0.cm";
		const std::string D = "Level_2/d.cm";
		const std::string BLANK_TILE = "Level_1/00_0000_0000_0000_0.cm";

		addProp("door.cm", 1, 1, 1.0f, 1.0f, 2.0f);
		addProp("sandpile.cm", 2, 1, 0.0f, 0.0f, 0.0f);
		addProp("vase.cm", 3, 1, 0.0f, 0.0f, 0.0f);
		addProp("vase.cm", 3, 1, 1.0f, 0.0f, 0.0f);
		addProp("vase.cm", 3, 1, 4.0f, 0.0f, 0.0f);
		addProp("vase.cm", 1, 1, -3.0f, 1.0f , 0.0f);

		addTile(BG, 0, 0);
		addTile(BG, 0, 1);
		addTile(BG, 0, 2);
		addTile(BG, 0, 3);

		addTile(BG, 1, 0);
		addTile(BG, 2, 0);
		addTile(BG, 3, 0);
		addTile(BG, 4, 0);

		addTile(BG, 1, 3);
		addTile(BG, 2, 3);
		addTile(BG, 3, 3);
		addTile(BG, 4, 3);

		addTile(BG, 4, 2);
		addTile(BG, 4, 1);
	
		addTile(CCCC, 1, 2);
		addTile(DDDD, 3, 2);
		addTile(D, 3, 1);
		addTile(BLANK_TILE, 2, 1);
		addTile(BLANK_TILE, 2, 2);
		addTile(AAAAA, 1, 1);

		collisions_.add(cl::OBB3D(getTilePosition(3, 1) + glm::vec3(3.0f, 0.0f, 0.0f), glm::vec3(1.0f, 6.0f, 6.0f)));
		collisions_.add(cl::OBB3D(getTilePosition(3, 1) + glm::vec3(-2.0f, 0.0f, 0.0f), glm::vec3(1.0f, 3.0f, 6.0f)));

		player_->setPosition(getTilePosition(1, 1) + glm::vec3(1.0f, 0.0f, 0.0f));
		camera_->snapTo(player_->getPhysicsPosition());
		camera_->moveTo(player_->getPhysicsPosition(), 1.0f);

		// Add Enemies
        addBoss(new Boss(*player_), 2, 2, 0.0f, 0.0f);



		// make sure these two lines are called; will be obvious if not since everything will crash
		EnemyManager::initEnemies(cache_);
		mergeCollisions();

		exit_area_.centre = getTilePosition(5, 2);
		exit_area_.centre.x += 0.0f;
		exit_area_.centre.y += 3.0f;
		exit_area_.radius = glm::vec3(1.0f);

		next_area_ = Level::LevelUpgrade;
	}

private:

};

