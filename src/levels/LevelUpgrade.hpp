#pragma once
#include "../Level.hpp"

class LevelUpgrade : public Level
{
public:
	LevelUpgrade()
	{
		const std::string AAA = "Upgrade_Room/aaa.cm";
		const std::string BBB = "Upgrade_Room/bbb.cm";
		const std::string BG = "Level_1/00_0000_0000_0000_1.cm";
		const std::string DOOR = "Props/door.cm";
		
		addProp("door.cm", 1, 0, 0.0f, 1.0f, 2.0f);
		addProp("door.cm", 0, 0, 0.0f, 1.0f, 2.0f);
		addUpgradeModel(0, 0, 8.0f, 0.0f, 0.0f);

		addTile(AAA, 0, 0);
		addTile(BBB, 1, 0);

		addTile(BG, -1, 0);
		addTile(BG, -1, 1);
		addTile(BG, 0, 1);
		addTile(BG, 1, 1);
		addTile(BG, 0, -1);
		addTile(BG, 1, -1);
		addTile(BG, -1, -1);
		addTile(BG, 2, -1);
		addTile(BG, 2, 0);
		addTile(BG, 2, 1);

		player_->setPosition(getTilePosition(0, 1) + glm::vec3(0.0f, -10.0f, 0.0f));
		camera_->snapTo(player_->getPhysicsPosition());
		camera_->moveTo(player_->getPhysicsPosition(), 1.0f);

		// Add Enemies


		// make sure these two lines are called; will be obvious if not since everything will crash
		EnemyManager::initEnemies(cache_);
		mergeCollisions();

		exit_area_.centre = getTilePosition(1, 0);
		exit_area_.centre.x += 0.0f;
		exit_area_.centre.y += 3.0f;
		exit_area_.radius = glm::vec3(1.0f);


		if (Level::PreviousLevel == Level::LevelStart)
		{
			next_area_ = Level::LevelOne;
		}
		else if (Level::PreviousLevel == Level::LevelOne)
		{
			next_area_ = Level::LevelTwo;
		}
		else if (Level::PreviousLevel == Level::LevelTwo)
		{
			next_area_ = Level::LevelThree;
		}
		else if (Level::PreviousLevel == Level::LevelThree)
		{
			next_area_ = Level::LevelFour;
		}
		else if (Level::PreviousLevel == Level::LevelFour)
		{
			next_area_ = Level::LevelFive;
		}
		//else if (Level::PreviousLevel == Level::LevelFive)
		//{
		//	next_area_ = Level::LevelBoss;
		//}
		
	}

private:

};