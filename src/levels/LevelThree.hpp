#pragma once
#include "../Level.hpp"
#include "../TestEnemy.hpp"
#include "../Dog.hpp"
#include "../Spearman.hpp"
#include "../Ranged.hpp"

class LevelThree : public Level
{
public:
	LevelThree()
	{
		const std::string AA = "Level_3/aa.cm";
		const std::string BB = "Level_3/bb.cm";
		const std::string CC = "Level_3/cc.cm";
		const std::string DD = "Level_3/dd.cm";
		const std::string EE = "Level_3/ee.cm";
		const std::string FF = "Level_3/ff.cm";
		const std::string GG = "Level_3/gg.cm";
		const std::string HH = "Level_3/hh.cm";
		const std::string II = "Level_3/ii.cm";
		const std::string JJ = "Level_3/jj.cm";
		const std::string KK = "Level_3/kk.cm";
		const std::string LL = "Level_3/ll.cm";
		const std::string BG = "Level_1/00_0000_0000_0000_1.cm";
		const std::string BLANK_TILE = "Level_1/00_0000_0000_0000_0.cm";

		addProp("door.cm", 2, 4, 0.0f, 1.0f, 2.0f);
		addProp("door.cm", 4, 1, -2.0f, 1.0f, 2.0f);

		addSpikes(1, 2, 0.0f, 0.0f, 6.0f, 3.0f);
		addSpikes(3, 2, 0.0f, 0.0f, 6.0f, 3.0f);

		addTile(BG, 0, 0);
		addTile(BG, 1, 0);
		addTile(BG, 1, 1);
		addTile(BG, 0, 1);
		addTile(BG, 0, 2);
		addTile(BG, 0, 3);
		addTile(BG, 0, 4);
		addTile(BG, 1, 4);

		addTile(AA, 2, 4);
		addTile(BG, 3, 4);
		addTile(BG, 0, 5);
		addTile(BG, 1, 5);
		addTile(BG, 2, 5);
		addTile(BG, 3, 5);

		addTile(DD, 1, 3);
		addTile(BB, 2, 3);
		addTile(CC, 3, 3);
		addTile(EE, 4, 3);

		addTile(EE, 1, 2);
		addTile(BLANK_TILE, 2, 2);
		addTile(FF, 3, 2);
		addTile(BG, 4, 2);
		addTile(BG, 5, 2);

		addTile(GG, 2, 1);
		addTile(BG, 3, 1);
		addTile(LL, 4, 1);
		addTile(KK, 5, 1);
		addTile(BG, 6, 1);

		addTile(HH, 2, 0);
		addTile(BB, 3, 0);
		addTile(II, 4, 0);
		addTile(JJ, 5, 0);
		addTile(BG, 6, 0);

		addTile(BG, 1, -1);
		addTile(BG, 2, -1);
		addTile(BG, 3, -1);
		addTile(BG, 4, -1);
		addTile(BG, 5, -1);
		addTile(BG, 6, -1);

		player_->setPosition(getTilePosition(2, 4) + glm::vec3(0.0f, 1.0f, 0.0f));
		camera_->snapTo(player_->getPhysicsPosition());
		camera_->moveTo(player_->getPhysicsPosition(), 1.0f);

		// Add Enemies

		addEnemy(new Oblisk(*player_), 1, 3, -5.0f, 2.0f);
		addEnemy(new Spearman(*player_), 2, 3, 0.0f, 0.0f);
		addEnemy(new Oblisk(*player_), 3, 3, 5.0f, 2.0f);
		addEnemy(new Bat(*player_), 2, 2, 0.0f, 5.0f);
		addEnemy(new Bat(*player_), 2, 2, 0.0f, 0.0f);
		addEnemy(new Spearman(*player_), 2, 1, -3.0f, 5.0f);
		addEnemy(new Spearman(*player_), 2, 1, 3.0f, 5.0f);
		addEnemy(new Oblisk(*player_), 2, 1, -6.0f, 5.0f);
		addEnemy(new Spearman(*player_), 3, 0, 3.0f, 5.0f);
		addEnemy(new Spearman(*player_), 4, 1, 0.0f, 5.0f);
		addEnemy(new Spearman(*player_), 5, 1, 0.0f, 5.0f);
		addEnemy(new Spearman(*player_), 3, 0, 0.0f, 0.0f);
		addEnemy(new Spearman(*player_), 4, 0, 0.0f, 0.0f);
		addEnemy(new Dog(*player_), 5, 0, -5.0f, 0.0f);
		addEnemy(new Dog(*player_), 5, 0, -3.0f, 0.0f);
		addEnemy(new Dog(*player_), 5, 0, -1.0f, 0.0f);
		addEnemy(new Dog(*player_), 5, 0, -2.0f, 0.0f);
		addEnemy(new Dog(*player_), 5, 0, -5.0f, 0.0f);
		addEnemy(new Dog(*player_), 5, 0, -4.0f, 0.0f);
		addEnemy(new Dog(*player_), 5, 0, -6.0f, 0.0f);

		// make sure these two lines are called; will be obvious if not since everything will crash
		EnemyManager::initEnemies(cache_);
		mergeCollisions();

		exit_area_.centre = getTilePosition(4, 1);
		exit_area_.centre.x += -2.0f;
		exit_area_.centre.y += 3.0f;
		exit_area_.radius = glm::vec3(1.0f);

		next_area_ = Level::LevelUpgrade;
	}

private:

};