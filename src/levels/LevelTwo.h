#pragma once
#include "../Level.hpp"
#include "../TestEnemy.hpp"
#include "../Dog.hpp"
#include "../Spearman.hpp"
#include "../Ranged.hpp"
#include "../SoundManager.hpp"

class LevelTwo : public Level
{
public:
	LevelTwo()
	{
		const std::string A = "Level_2/a.cm";
		const std::string B = "Level_2/b.cm";
		const std::string C = "Level_2/c.cm";
		const std::string D = "Level_2/d.cm";
		const std::string E = "Level_2/e.cm";
		const std::string F = "Level_2/f.cm";
		const std::string G = "Level_2/g.cm";
		const std::string H = "Level_2/h.cm";
		const std::string BG = "Level_1/00_0000_0000_0000_1.cm";

		addProp("door.cm", 7, 1, 0.0f, 1.0f, 2.0f);
		addProp("door.cm", 0, 6, 0.0f, 1.0f, 2.0f);

		addSpikes(0, 2, 0.0f, 0.0f, 6.0f, 3.0f);
		addSpikes(1, 1, 0.0f, 0.0f, 6.0f, 3.0f);

		addTile(BG, 0, 0);
		addTile(BG, 1, 0);
		addTile(BG, 2, 0);
		addTile(BG, 3, 0);
		addTile(BG, 4, 0);
		addTile(BG, 5, 0);
		addTile(BG, 6, 0);
		addTile(BG, 7, 0);
		addTile(BG, 8, 0);
		
		addTile(BG, 0, 1);
		addTile(E, 1, 1);
		addTile(G, 2, 1);
		addTile(G, 3, 1);
		addTile(G, 4, 1);
		addTile(G, 5, 1);
		addTile(G, 6, 1);
		addTile(H, 7, 1);
		addTile(BG, 8, 1);
		addTile(BG, 8, 2);
		

		addTile(E, 0, 2);
		addTile(D, 1, 2);
		addTile(BG, 2, 2);
		addTile(BG, 3, 2);
		addTile(BG, 4, 2);
		addTile(BG, 5, 2);
		addTile(BG, 6, 2);
		addTile(BG, 7, 2);
	

		addTile(C, 0, 3);
		addTile(D, 1, 3);
		addTile(BG, 2, 3);

		addTile(C, 0, 4);
		addTile(D, 1, 4);
		addTile(BG, 2, 4);

		addTile(C, 0, 5);
		addTile(D, 1, 5);
		addTile(BG, 2, 5);

		addTile(A, 0, 6);
		addTile(B, 1, 6);
		addTile(BG, 2, 6);

		addTile(BG, -1, 6);
		addTile(BG, -1, 5);
		addTile(BG, -1, 4);
		addTile(BG, -1, 3);
		addTile(BG, -1, 2);
		addTile(BG, -1, 1);

		addTile(BG, 0, 7);
		addTile(BG, 1, 7);
		addTile(BG, 2, 7);
		addTile(BG, -1, 7);



		player_->setPosition(getTilePosition(0, 6) + glm::vec3(0.0f, 1.0f, 0.0f));
		camera_->snapTo(player_->getPhysicsPosition());
		camera_->moveTo(player_->getPhysicsPosition(), 1.0f);

		// Add Enemies

		addEnemy(new Oblisk(*player_), 0, 5, -5.0f, 2.0f);
		addEnemy(new Oblisk(*player_), 0, 4, -5.0f, 2.0f);
		addEnemy(new Oblisk(*player_), 0, 3, -5.0f, 2.0f);
		addEnemy(new Bat(*player_), 1, 6, 0.0f, 5.0f);
		addEnemy(new Bat(*player_), 0, 3, 0.0f, 0.0f);
		addEnemy(new Bat(*player_), 0, 3, 0.0f, 2.0f);
		addEnemy(new Bat(*player_), 2, 0, 0.0f, 0.0f); 
		addEnemy(new Bat(*player_), 2, 0, 0.0f, 4.0f);
		addEnemy(new Bat(*player_), 2, 0, 5.0f, 4.0f);
		addEnemy(new Bat(*player_), 2, 1, 0.0f, 4.0f);
		addEnemy(new Dog(*player_), 3, 1, 0.0f, 4.0f);
		addEnemy(new Oblisk(*player_), 3, 1, 0.0f, 4.0f);
		addEnemy(new Dog(*player_), 3, 1, 5.0f, 4.0f);
		addEnemy(new Dog(*player_), 4, 1, 0.0f, 4.0f);
		addEnemy(new Bat(*player_), 4, 1, 0.0f, 4.0f);
		addEnemy(new Spearman(*player_), 4, 1, 0.0f, 4.0f);
		addEnemy(new Dog(*player_), 4, 1, 5.0f, 4.0f);
		addEnemy(new Dog(*player_), 5, 1, 0.0f, 4.0f);
		addEnemy(new Oblisk(*player_), 5, 1, 0.0f, 4.0f);
		addEnemy(new Dog(*player_), 5, 1, 5.0f, 4.0f);
		addEnemy(new Dog(*player_), 6, 1, 0.0f, 4.0f);
		addEnemy(new Bat(*player_), 6, 1, 0.0f, 4.0f);
		addEnemy(new Spearman(*player_), 6, 1, 0.0f, 4.0f);
		addEnemy(new Dog(*player_), 6, 1, 5.0f, 4.0f);
	


		// make sure these two lines are called; will be obvious if not since everything will crash
		EnemyManager::initEnemies(cache_);
		mergeCollisions();

		exit_area_.centre = getTilePosition(7, 1);
		exit_area_.centre.x += 0.0f;
		exit_area_.centre.y += 3.0f;
		exit_area_.radius = glm::vec3(1.0f);

		next_area_ = Level::LevelUpgrade;
	}

private:

};
