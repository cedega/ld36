#pragma once
#include "../Level.hpp"
#include "../TestEnemy.hpp"
#include "../Dog.hpp"
#include "../Spearman.hpp"
#include "../Ranged.hpp"
#include "../Boss.hpp"
#include "../SoundManager.hpp"

class LevelOne: public Level
{
public:
    LevelOne()
    {
		SoundManager::stopMusic();
		SoundManager::playMusic(SoundManager::Tech, 15);
        const std::string BG = "Level_1/00_0000_0000_0000_1.cm";
        const std::string BLANK_TILE = "Level_1/00_0000_0000_0000_0.cm";
        const std::string WALKWAY = "Level_1/00_0010_0000_0000_0.cm";
        const std::string TLCORNER = "Level_1/10_0100_0000_1000_0.cm";
		const std::string START = "Level_1/01_0010_0000_0000_0.cm";
		const std::string FINISH = "Level_1/01_0001_0000_0000_0.cm";
		const std::string WARNING = "Level_1/11_0000_0010_0001_0.cm";
		const std::string BASIC_1 = "Level_1/10_0010_1000_0001_0.cm";
		const std::string DOWN_1 = "Level_1/11_1000_0100_1000_0.cm";
		const std::string SPIKE = "Level_1/01_0000_0000_0000_0.cm";

		addProp("door.cm", 3, 3, 0.0f, 1.0f, 2.0f);
		addProp("door.cm", 3, 5, 0.0f, 1.0f, 2.0f);

		addSpikes(3, 2, 0.0f, 0.0f, 6.0f, 3.0f);

        addTile(BG, 0, 0);
		addTile(BG, 1, 0);
		addTile(BG, 2, 0);
		addTile(BG, 3, 0);
		addTile(BG, 4, 0);

		addTile(BG, 0, 1);
		addTile(BG, 0, 2);
		addTile(BG, 0, 3);
		addTile(BG, 0, 4);
		addTile(BG, 0, 5);
		addTile(BG, 0, 6);

		addTile(BG, 1, 6);
		addTile(BG, 2, 6);
		addTile(BG, 3, 6);
		addTile(BG, 4, 6);

		addTile(BG, 4, 5);
		addTile(BG, 4, 4);
		addTile(BG, 4, 3);
		addTile(BG, 4, 2);
		addTile(BG, 4, 1);
		addTile(BG, 4, 0);

		addTile(BG, 1, 1);
		addTile(BG, 2, 1);
		addTile(BG, 3, 1);

		addTile(BG, 1, 2);

		addTile(BG, 2, 4);
		addTile(BG, 3, 4);

		addTile(START, 3, 5);
		addTile(FINISH, 3, 3);

		addTile(WALKWAY, 2, 5);

		addTile(TLCORNER, 1, 5);

		addTile(DOWN_1, 1, 4);

		addTile(BASIC_1, 1, 3);

		addTile(BLANK_TILE, 2, 3);

		addTile(WARNING, 2, 2);

		addTile(SPIKE, 3, 2);

		player_->setPosition(getTilePosition(3, 5) + glm::vec3(0.0f, 0.0f, 0.0f));
        camera_->snapTo(player_->getPhysicsPosition());
        camera_->moveTo(player_->getPhysicsPosition(), 1.0f);

		 //Add Enemies

		addEnemy(new Dog(*player_), 2, 5, 0.0f, 5.0f);
		addEnemy(new Dog(*player_), 2, 5, 0.0f, 0.0f);
		addEnemy(new Dog(*player_), 2, 5, 4.0f, 0.0f); 
		addEnemy(new Bat(*player_), 2, 5, 5.0f, 0.0f);
		addEnemy(new Spearman(*player_), 2, 5, -10.0f, 0.0f);
		addEnemy(new Spearman(*player_), 2, 2, -10.0f, 5.0f);
		addEnemy(new Dog(*player_), 2, 5, -10.0f, 5.0f);
		addEnemy(new Bat(*player_), 1, 5, -5.0f, 8.0f);
		addEnemy(new Oblisk(*player_), 1, 4, -5.0f, 5.0f);
		addEnemy(new Oblisk(*player_), 1, 4, -3.0f, 5.0f);
		addEnemy(new Bat(*player_), 3, 3, -5.0f, 8.0f);
		addEnemy(new Bat(*player_), 3, 2, 8.0f, -6.0f);
		addEnemy(new Dog(*player_), 3, 2, 7.0f, -5.0f);
		addEnemy(new Oblisk(*player_), 1, 4, -5.0f, 0.0f);
		addEnemy(new Dog(*player_), 2, 2, 0.0f, 5.0f);
		addEnemy(new Bat(*player_), 1, 4, 0.0f, 0.0f);
		addEnemy(new Oblisk(*player_), 1, 5, -5.0f, 0.0f);


		// make sure these two lines are called; will be obvious if not since everything will crash
		EnemyManager::initEnemies(cache_);
        mergeCollisions();

        exit_area_.centre = getTilePosition(3, 3);
        exit_area_.centre.x += 0.0f;
        exit_area_.centre.y += 3.0f;
        exit_area_.radius = glm::vec3(1.0f);

		next_area_ = Level::LevelUpgrade;
    }

private:

};
