#pragma once
#include "../Projectile.hpp"
#include "../ShaderManager.hpp"

class Arrow : public Projectile
{
public:
    Arrow()
    {
        cl::AssetLoader loader;
        cl::AssetLoader::TextureArguments texargs;
        cl::Texture *texture = loader.loadTexture("data/projectiles/energyarrow.png", texargs, cache_);
        cl::AnimatedSpriteSheet::initialize(texture, ShaderManager::shader2d, false, glm::ivec2(128), 2);
        cl::AnimatedSpriteSheet::createAnimation("moving", 1, 1, 1.0f);
        cl::AnimatedSpriteSheet::createAnimation("death", 2, 3, 100.0f / 1000.0f);
        cl::AnimatedSpriteSheet::setLooping("moving", false);
        cl::AnimatedSpriteSheet::setLooping("death", false);
        cl::AnimatedSpriteSheet::loadAnimation("moving");

        hitbox_.radius = glm::vec3(0.5f, 0.2f, 0.2f);
    }

    void create(const glm::vec3 &position, const glm::vec3 &direction, bool target_enemy)
    {
        setPosition(position);
        setRotation(glm::vec3(0.0f, 0.0f, getRotationFromDirection(direction)), true);
        hitbox_.setRotation(getRotation());
        direction_ = direction;
        target_enemy_ = target_enemy;

        setScaling(glm::vec3(0.03f * 0.65f), true);
        setSpeed(20.0f);

        setDamage(10);
    }

    void processAnimations()
    {
        if (isAnimating("death"))
        {
            if (currentAnimationFinished())
            {
                destroy_ = true;
            }
        }
        else if (!isAlive())
        {
            cl::AnimatedSpriteSheet::loadAnimation("death");
        }
    }

private:

};
