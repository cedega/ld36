#pragma once
#include "../Projectile.hpp"
#include "../ShaderManager.hpp"
#include "../Enemy.hpp"

class ObeliskShot : public Projectile
{
public:
    ObeliskShot()
    {
        cl::AssetLoader loader;
        cl::AssetLoader::TextureArguments texargs;
        cl::Texture *texture = loader.loadTexture("data/projectiles/obliskshot.png", texargs, cache_);
        cl::AnimatedSpriteSheet::initialize(texture, ShaderManager::shader2d, false, glm::ivec2(128), 2);
        cl::AnimatedSpriteSheet::createAnimation("moving", 1, 1, 1.0f);
        cl::AnimatedSpriteSheet::createAnimation("death", 2, 3, 100.0f / 1000.0f);
        cl::AnimatedSpriteSheet::setLooping("moving", false);
        cl::AnimatedSpriteSheet::setLooping("death", false);
        cl::AnimatedSpriteSheet::loadAnimation("moving");

        hitbox_.radius = glm::vec3(0.3f, 0.3f, 0.3f);
    }

    void create(const glm::vec3 &position, const glm::vec3 &direction, bool target_enemy)
    {
        setPosition(position);
        setRotation(glm::vec3(0.0f, 0.0f, getRotationFromDirection(direction)), true);
        hitbox_.setRotation(getRotation());
        direction_ = direction;
        target_enemy_ = target_enemy;
        life_time_ = 2.0f;

        setScaling(glm::vec3(0.03f), true);
        setSpeed(20.0f);

        setDamage(static_cast<int>(Enemy::getStatScale() * 5));
    }

    void processAnimations()
    {
        if (isAnimating("death"))
        {
            if (currentAnimationFinished())
            {
                destroy_ = true;
            }
        }
        else if (!isAlive())
        {
            cl::AnimatedSpriteSheet::loadAnimation("death");
        }
    }

private:

};
