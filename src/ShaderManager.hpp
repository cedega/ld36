#pragma once
#include <Clone/Graphics/GraphicsAPI.hpp>

class ShaderManager
{
public:
    static void initialize();
    static void destroy();
    static cl::Shader *shader2d;
    static cl::Shader *shader3d;
    static cl::Shader *particles;
    static cl::Shader *debug;

private:
    ShaderManager() {}

};