#include "Projectile.hpp"
#include "Player.hpp"
#include "EnemyManager.hpp"

cl::AssetCache *Projectile::cache_ = NULL;
Player *Projectile::player_ = NULL;

Projectile::Projectile() :
    collisions_(NULL),
    speed_(0.0f),
    damage_(0),
    life_tick_(0.0f),
    life_time_(0.75f),
    alive_(true),
    destroy_(false),
    target_enemy_(true)
{
    physics_.setGravity(glm::vec3(0.0f));
}

void Projectile::update(float dt)
{
    cl::AnimatedSpriteSheet::updateLastState();

    physics_.setVelocity(direction_ * speed_);

    if (!isAlive())
        physics_.setVelocity(glm::vec3(0.0f));

    physics_.integrate(dt);

    hitbox_.centre = physics_.getPosition();

    for (unsigned int i = 0; i < collisions_->getOBBs().size(); i++)
    {
        glm::vec3 mtv;
        if (cl::intersect3DSAT(hitbox_, collisions_->getOBBs()[i], &mtv))
        {
            // Destroy projectile
            hitbox_.centre += mtv;
            alive_ = false;
            break;
        }
    }

    // If we're not suppose to kill the particle, process enemy hit detection
    if (isAlive())
    {
        if (target_enemy_)
        {
            for (unsigned int i = 0; i < EnemyManager::getEnemies().size(); i++)
            {
                if (EnemyManager::getEnemies()[i]->isAlive())
                {
                    glm::vec3 mtv;
                    if (cl::intersect3DSAT(hitbox_, EnemyManager::getEnemies()[i]->getHitbox(), &mtv))
                    {
                        hitbox_.centre += mtv;
                        EnemyManager::getEnemies()[i]->damage(getDamage());
                        alive_ = false;
                        break;
                    }
                }
            }
        }
        else
        {
            if (player_->isAlive())
            {
                glm::vec3 mtv;
                if (cl::intersect3DSAT(hitbox_, player_->getHitbox(), &mtv))
                {
                    hitbox_.centre += mtv;
                    player_->damage(getDamage());
                    alive_ = false;
                }
            }
        }
    }

    life_tick_ += dt;
    if (life_tick_ >= life_time_)
    {
        alive_ = false;
    }

    // cl::Log(cl::CL_LOG_INFO, "%s", cl::toStringV3(hitbox_.centre).c_str());
    physics_.setPosition(hitbox_.centre);
    cl::AnimatedSpriteSheet::setPosition(hitbox_.centre + hitbox_offset_, false);
    processAnimations();
    cl::AnimatedSpriteSheet::update(dt);

    if (!isAlive())
        cl::AnimatedSpriteSheet::updateLastState();
}

void Projectile::setPosition(const glm::vec3 &pos)
{
    physics_.setPosition(pos);
    cl::AnimatedSpriteSheet::setPosition(pos, true);
}

void Projectile::setCollisions(Collisions *collisions)
{
    collisions_ = collisions;
}

float Projectile::getRotationFromDirection(const glm::vec3 &direction)
{
    return atan2f(direction.y, direction.x);
}

void Projectile::setSpeed(float speed)
{
    speed_ = speed;
}

float Projectile::getSpeed() const
{
    return speed_;
}

void Projectile::setDamage(int damage)
{
    damage_ = damage;
}

int Projectile::getDamage() const
{
    return damage_;
}

const cl::OBB3D& Projectile::getHitbox() const
{
    return hitbox_;
}

bool Projectile::isAlive() const
{
    return alive_;
}

bool Projectile::shouldDestroy() const
{
    return destroy_;
}