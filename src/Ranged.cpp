#include "Ranged.hpp"
#include "RangedAttack.hpp"

void Ranged::initialize(cl::AssetCache* cache)
{
	cl::AssetLoader loader;
	cl::AssetLoader::TextureArguments texargs;
	texargs.loader_args = cl::AssetLoader::IR;
	texargs.min_filter = cl::Texture::Linear;
	texargs.mag_filter = cl::Texture::Linear;
	cl::Texture *texture = loader.loadTexture("data/ranged_man/rangedman.png", texargs, cache);
	cl::AnimatedSpriteSheet::initialize(texture, ShaderManager::shader2d, false, glm::ivec2(200, 200), 2);

	cl::AnimatedSpriteSheet::createAnimation("run", 1, 10, 50 / 1000.0f);
	cl::AnimatedSpriteSheet::createAnimation("attack", 11, 16, 50 / 1000.0f);
	cl::AnimatedSpriteSheet::createAnimation("hit", 17, 17, 50 / 1000.0f);
	cl::AnimatedSpriteSheet::createAnimation("death", 18, 20, 50 / 1000.0f);
	cl::AnimatedSpriteSheet::createAnimation("jump", 8, 8, 50 / 1000.0f);

	cl::AnimatedSpriteSheet::setLooping("hit", false);
	cl::AnimatedSpriteSheet::setLooping("death", false);
	cl::AnimatedSpriteSheet::setLooping("jump", false);
	cl::AnimatedSpriteSheet::setLooping("attack", false);

	cl::AnimatedSpriteSheet::loadAnimation("run");

	Entity::setHitboxOffset(glm::vec3(0.0f, 1.0f, 0.25f));
	hitbox_.radius = glm::vec3(0.9f, 1.25f, 0.1f);
	cl::AnimatedSpriteSheet::setScaling(glm::vec3(0.03f * 0.75f), true);

    setHealth(static_cast<int>(70 * scaling_));
	setSpeed(3.0f);
	setJump(5.0f);
	setDamage(scaling_ * 12.5f);
	addStrategy(new RangedAttack());
}

void Ranged::update(float dt)
{
	glm::vec3 pPos = player_.getPosition();
	glm::vec3 ePos = getPosition();
	float delta = 16.0f;
	attack_tick_ += dt;
	if (getStrategy(state::STRAT_CHASE) && glm::distance(ePos, pPos) < delta && glm::distance(ePos, pPos) > range_)
	{
		setStrategy(state::STRAT_CHASE);
	}
	else if (getStrategy(state::STRAT_ATTACK) && attack_tick_ > attack_timer_ && glm::distance(ePos, pPos) < range_)
	{
		setStrategy(state::STRAT_ATTACK);
		attack_tick_ = 0.0f;
	}
	else
	{
		setStrategy(state::STAND);
	}
	Enemy::update(dt);
}

void Ranged::processAnimations()
{
	Enemy::processAnimations();
	if (attack_tick_ < 0.5f && !isAnimating("attack"))
	{
		cl::AnimatedSpriteSheet::loadAnimation("attack");
	}
}