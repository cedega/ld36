#pragma once
#include "Enemy.hpp"

class Dog : public Enemy
{
public:
	Dog(Player& p) : Enemy(p) {};
	Dog(glm::vec3 pos, Player& p) : Enemy(p) { setPosition(pos); };

	void initialize(cl::AssetCache* cache);
	void update(float dt);
	void processAnimations();
	void playAttackSound();
};