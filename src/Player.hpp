#pragma once
#include "Entity.hpp"
#include "projectiles/Arrow.hpp"
#include <Clone/Graphics/Renderer.hpp>
#include "SoundManager.hpp"

class Player : public Entity
{
public:
   Player();

   void initialize(cl::AssetCache *cache);
   void input();
   bool inputUpgrades();
   void update(float dt);
   void render(cl::Renderer *renderer);
   void reset();
   int getKillNumber();
   void incrementKillNumber();
   void resetKillNumber();

   int getUpgradeSelectIndex() const { return upgrade_select_index_; }
   void setAttackSpeed(float time);

protected:
    virtual bool isPressingLeft();
    virtual bool isPressingRight();
    virtual bool isPressingJump();
    virtual void processAnimations();

private:
    struct PlayerInput
    {
        bool up;
        bool down;
        bool left;
        bool right;
        bool jump;
        bool shoot;

        PlayerInput() :
            up(false),
            down(false),
            left(false),
            right(false),
            jump(false),
            shoot(false)
        {
        }
    };

    PlayerInput inputs_;
    PlayerInput last_inputs_;

    int last_attack_frame_;
    bool attack_;
	bool played_death_sound_;
	bool sound_is_not_playing_;
    cl::AnimatedSpriteSheet lower_body_;
    int upgrade_select_index_;
	int kill_number_;
};