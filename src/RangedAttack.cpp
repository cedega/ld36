#include "RangedAttack.hpp"
#include "projectiles/Spear.hpp"

RangedAttack::RangedAttack()
{
	state_ = state::STRAT_ATTACK;
}

void RangedAttack::move(Enemy& e, Player& p)
{
	glm::vec3 ePos = e.getPosition();
	glm::vec3 pPos = p.getPosition();
	float delta = 5.0f;
	e.input_.left = false;
	e.input_.right = false;
	e.input_.up = false;
	if (e.isAlive())
	{
		const glm::vec3 offset(e.getScaling().x > 0.1f ? -0.1f : 0.0f, 0.5f, 0.0f);
		Projectile *proj = new Spear();

		glm::vec3 direction = glm::normalize(pPos - ePos);
		proj->create(ePos + offset, direction, false);

		proj->setDamage(static_cast<int>(proj->getDamage()));
		proj->setSpeed(10.0f);

		e.getProjectiles().push_back(proj);
	}
}