#include "PatrolStrategy.hpp"

PatrolStrategy::PatrolStrategy(const glm::vec3 pt1, const glm::vec3 pt2) :
	point1_(pt1),
	point2_(pt2)
{
	currentDestination_ = point1_;
	state_ = state::STRAT_PATROL;
}

void PatrolStrategy::move(Enemy& e, Player& p)
{
	glm::vec3 pos = e.getPosition();
	float delta = 1.0f;
	// if we're at at out destination, change patrol points
	if (glm::abs(pos - currentDestination_).x < delta)
	{
		currentDestination_ = (currentDestination_ == point1_) ? point2_ : point1_;
	}
	if (pos.x < currentDestination_.x)
	{ 
		e.input_.right = true;
		e.input_.left = false;
	}
	else
	{
		e.input_.right = false;
		e.input_.left = true;
	}
}

void PatrolStrategy::attack()
{
	// do nothing
}

void PatrolStrategy::die()
{
	// drop some goo
}