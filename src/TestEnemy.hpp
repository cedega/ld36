#pragma once
#include "Enemy.hpp"


class TestEnemy : public Enemy
{
public:
	TestEnemy(Player& p) : Enemy(p) {};
	TestEnemy(glm::vec3 pos, Player& p) : Enemy(p) { setPosition(pos); };
	void initialize(cl::AssetCache* cache);
	void update(float dt);
};
