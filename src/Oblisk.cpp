#include "Oblisk.hpp"
#include "projectiles/ObeliskShot.hpp"

void Oblisk::initialize(cl::AssetCache* cache)
{
	cl::AssetLoader loader;
	cl::AssetLoader::TextureArguments texargs;
	texargs.loader_args = cl::AssetLoader::IR;
	texargs.min_filter = cl::Texture::Linear;
	texargs.mag_filter = cl::Texture::Linear;
	cl::Texture *texture = loader.loadTexture("data/oblisk/oblisk.png", texargs, cache);
	cl::AnimatedSpriteSheet::initialize(texture, ShaderManager::shader2d, false, glm::ivec2(200, 200), 2);

	cl::AnimatedSpriteSheet::createAnimation("run", 1, 1, 50 / 1000.0f);
	cl::AnimatedSpriteSheet::createAnimation("hit", 9, 9, 50 / 1000.0f);
	cl::AnimatedSpriteSheet::createAnimation("attacking", 2, 2, 50 / 1000.0f);
	cl::AnimatedSpriteSheet::createAnimation("death", 3, 8, 50 / 1000.0f);

	cl::AnimatedSpriteSheet::setLooping("death", false);
	cl::AnimatedSpriteSheet::setLooping("run", true);

	cl::AnimatedSpriteSheet::loadAnimation("hit");

	Entity::setHitboxOffset(glm::vec3(0.00f, 0.00f, 0.25f));
	hitbox_.radius = glm::vec3(0.8f, 2.20f, 0.3f);
	cl::AnimatedSpriteSheet::setScaling(glm::vec3(0.03f), true);

    setHealth(static_cast<int>(40 * scaling_));
	setSpeed(0.0f);
	setJump(0.0f);
	setDamage(scaling_ * 10.0f);
}

void Oblisk::update(float dt)
{
	if (getStrategy(state::STRAT_CHASE))
	{
		setStrategy(state::STRAT_CHASE);
	}
	glm::vec3 pPos = player_.getPosition();
	glm::vec3 ePos = getPosition() + glm::vec3(0.0f, 1.5f, 0.0f);
	float delta = 16.0f;
	attack_tick_ += dt;
	if (glm::length(pPos - ePos) < delta && isAlive() && attack_tick_ > attack_timer_)
	{
		const glm::vec3 offset(getScaling().x > 0.1f ? -0.1f : 0.0f, 0.5f, 0.0f);
		Projectile *proj = new ObeliskShot();

        glm::vec3 direction = glm::normalize(pPos - ePos);
		proj->create(ePos + offset, direction, false);

		proj->setDamage(static_cast<int>(proj->getDamage() ));
		proj->setSpeed(10.0f);

		projectiles_.push_back(proj);
		SoundManager::playSound(SoundManager::EnergyArrow, 20);
		attack_tick_ = 0.0f;
	}
	Enemy::update(dt);
}

void Oblisk::processAnimations()
{
	Enemy::processAnimations();

	if (isAnimating("death") && sound_is_not_playing_)
	{
		SoundManager::playSound(SoundManager::Break, 100);
		SoundManager::playSound(SoundManager::EnemyHit);
		sound_is_not_playing_ = false;
	}
	else if (isAnimating("hit") && sound_is_not_playing_)
	{
		SoundManager::playSound(SoundManager::EnemyHit);
		sound_is_not_playing_ = false;
	}
	else if (isAnimating("run"))
		sound_is_not_playing_ = true;
	else
		sound_is_not_playing_ = false;
}
