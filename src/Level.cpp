#include "Level.hpp"
#include "ShaderManager.hpp"
#include "Upgrades.hpp"
#include "BossStrategy.hpp"

Level::Enum Level::PreviousLevel = Level::Debug;
Player *Level::player_ = NULL;
cl::AssetCache *Level::cache_ = NULL;
cl::Camera *Level::camera_ = NULL;

Level::Level() :
    upgrade_present_(false),
    exiting_(false),
    show_upgrades_(false),
    upgraded_(false),
	played_key_sound_(false)
{
    tiles_.reserve(64);
    props_.reserve(32);
    spikes_.reserve(8);

    Upgrades::Power values[3];
    values[0] = Upgrades::MaxUpgrades;
    values[1] = Upgrades::MaxUpgrades;
    values[2] = Upgrades::MaxUpgrades;

    Upgrades::Power generated;
    int index = 0;

    do
    {
        generated = static_cast<Upgrades::Power>(cl::random(0, Upgrades::MaxUpgrades - 1));

        if (!Upgrades::isUpgraded(generated) && values[0] != generated && values[1] != generated && values[2] != generated)
        {
            values[index] = generated;
            index++;
        }
    } 
    while (index <= 2);

    upgrades_[0] = values[0];
    upgrades_[1] = values[1];
    upgrades_[2] = values[2];
}

Level::~Level()
{
    for (unsigned int i = 0; i < tiles_.size(); i++)
        delete tiles_[i];
}

void Level::addUpgradeModel(int tile_x, int tile_y, float x, float y, float z)
{
    cl::AssetLoader loader;
    cl::AssetLoader::TextureArguments texargs;
    texargs.min_filter = cl::Texture::Linear;
    texargs.mag_filter = cl::Texture::Linear;
    cl::ModelData *data = loader.loadModelData("data/tiles/upgrade.cm", texargs, cache_);
    upgrade_model_.initialize(data, ShaderManager::shader3d);
    upgrade_model_.setPosition(glm::vec3((tile_x * 16.0f) + x, (tile_y * 9.0f) + y, -z), true);
    upgrade_model_.setScaling(glm::vec3(0.85f), true);
    upgrade_model_.setBackfaceCulling(false);
    upgrade_present_ = true;
    upgrade_area_.centre = upgrade_model_.getPosition() + glm::vec3(0.0f, 2.2f, 2.0f);
    upgrade_area_.radius = glm::vec3(0.5f, 2.5f, 4.0f);
}

void Level::addProp(const std::string &file_name, int tile_x, int tile_y, float x, float y, float z)
{
    cl::Model prop;
    cl::AssetLoader loader;
    cl::AssetLoader::TextureArguments texargs;
    texargs.min_filter = cl::Texture::Linear;
    texargs.mag_filter = cl::Texture::Linear;
    cl::ModelData *data = loader.loadModelData("data/tiles/" + file_name, texargs, cache_);
    prop.initialize(data, ShaderManager::shader3d);
    prop.setPosition(glm::vec3((tile_x * 16.0f) + x, (tile_y * 9.0f) + y, -z), true);
    prop.setScaling(glm::vec3(1.0f), true);
    prop.setRotation(glm::vec3(-M_PI / 2.0f, 0.0f, 0.0f), true);
    prop.setCullAABB(cl::AABB3D(glm::vec3(0.0f), glm::vec3(6.0f)));
    props_.push_back(prop);
}

void Level::addSpikes(int tile_x, int tile_y, float x, float y, float width, float height)
{
    cl::OBB3D spike;
    spike.centre = glm::vec3((tile_x * 16.0f) + x, (tile_y * 9.0f) + y, 0.0f);
    spike.radius = glm::vec3(width, height, 6.0f);
    spikes_.push_back(spike);
}

Enemy* Level::addBoss(Enemy *enemy, int tile_x, int tile_y, float x, float y)
{
    enemy->setPosition(getTilePosition(tile_x, tile_y) + glm::vec3(x, y, 0.0f));
    enemy->addStrategy(new BossStrategy());
    EnemyManager::addEnemy(enemy); 
    return enemy;
}

Enemy* Level::addEnemy(Enemy *enemy, int tile_x, int tile_y, float x, float y)
{
    enemy->setPosition(getTilePosition(tile_x, tile_y) + glm::vec3(x, y, 0.0f));
    enemy->addStrategy(new ChaseStrategy());
    EnemyManager::addEnemy(enemy); 
    return enemy;
}

void Level::addTile(const std::string &file_name, int x, int y)
{
    Tile *new_tile = new Tile();
    new_tile->setPosition(glm::vec3(x * 16.0f, y * 9.0f, 0.0f), true);
    new_tile->initialize(file_name, cache_);
    new_tile->setRotation(glm::vec3(-M_PI / 2.0f, 0.0f, 0.0f), true);
    new_tile->setScaling(glm::vec3(1.0001f), true);
    new_tile->setCullAABB(cl::AABB3D(glm::vec3(0.0f), glm::vec3(16.01f, 9.01f, 6.01f)));
    tiles_.push_back(new_tile);
}

void Level::mergeCollisions()
{
    for (unsigned int i = 0; i < tiles_.size(); i++)
    {
        for (unsigned int j = 0; j < tiles_[i]->getCollisions().size(); j++)
        {
            collisions_.add(tiles_[i]->getCollisions()[j]);
        }
    }

    player_->setCollisions(&collisions_);
	for (std::vector<Enemy*>::iterator enemy = EnemyManager::getEnemies().begin(); enemy != EnemyManager::getEnemies().end(); enemy++)
	{
		(*enemy)->setCollisions(&collisions_);
	}
}

void Level::update(float dt)
{
    player_->update(dt);
	for (std::vector<Enemy*>::iterator enemy = EnemyManager::getEnemies().begin(); enemy != EnemyManager::getEnemies().end(); enemy++)
	{
		(*enemy)->update(dt);
	}

    for (unsigned int i = 0; i < spikes_.size(); i++)
    {
        if (cl::intersect3DSAT(player_->getHitbox(), spikes_[i], NULL))
        {
            player_->damage(1000);
        }
    }
    
    std::vector<Projectile*> &pp = player_->getProjectiles();

    for (int i = 0; i < static_cast<int>(pp.size()); i++)
    {
        Projectile *proj = pp[i];

        proj->setCollisions(&collisions_);
        proj->update(dt);

        if (proj->shouldDestroy())
        {
            delete proj;
            pp.erase(pp.begin() + i);
            i--;
        }
    }

    for (unsigned int i = 0; i < EnemyManager::getEnemies().size(); i++)
    {
        Enemy *enemy = EnemyManager::getEnemies()[i];
        std::vector<Projectile*> &epp = enemy->getProjectiles();

        for (int j = 0; j < epp.size(); j++)
        {
            Projectile *proj = epp[j];

            proj->setCollisions(&collisions_);
            proj->update(dt);

            if (proj->shouldDestroy())
            {
                delete proj;
                epp.erase(epp.begin() + j);
                j--;
            }
        }
    }

    if (!upgraded_ && upgrade_present_ && cl::intersect3DSAT(player_->getHitbox(), upgrade_area_, NULL))
    {
        show_upgrades_ = true;
        Entity::setDisabled(true);
    }

    if (show_upgrades_)
    {
        bool pressed = player_->inputUpgrades();
        Upgrades::Power selected_power = upgrades_[player_->getUpgradeSelectIndex()];

        if (pressed)
        {
			SoundManager::playSound(SoundManager::Upgrade, 40);

            Upgrades::add(selected_power);
            Entity::setDisabled(false);
            upgraded_ = true;
            show_upgrades_ = false;

            // Heal the player
            int added_life = 0;
            if (Upgrades::isUpgraded(Upgrades::Health))
                added_life += 35;
            if (Upgrades::isUpgraded(Upgrades::GreaterHealth))
                added_life += 65;

            player_->setMaxHealth(75 + added_life);
            player_->setHealth(player_->getMaxHealth());

            // Set potentially new speed
            float added_speed = 0.0f;
            if (Upgrades::isUpgraded(Upgrades::MoveSpeed))
                added_speed += 3.0f;
            if (Upgrades::isUpgraded(Upgrades::GreaterMoveSpeed))
                added_speed += 5.0f;

            player_->setSpeed(9.9f + added_speed);

            // Set potentially new attack speed
            float attack_speed = 0.0f;
            if (Upgrades::isUpgraded(Upgrades::AttackSpeed))
                attack_speed -= 10.0f / 1000.0f;
            if (Upgrades::isUpgraded(Upgrades::GreaterAttackSpeed))
                attack_speed -= 15.0f / 1000.0f;

            player_->setAttackSpeed(50.0f / 1000.0f + attack_speed);
        }
    }

    if (canProgress() && cl::intersect3DSAT(player_->getHitbox(), exit_area_, NULL))
    {
        exiting_ = true;
    }

    // cl::Log(cl::CL_LOG_INFO, "%i", static_cast<int>(pp.size()));
}

void Level::render(cl::Renderer *renderer)
{
    player_->render(renderer);

    for (unsigned int i = 0; i < player_->getProjectiles().size(); i++)
    {
        renderer->draw(player_->getProjectiles()[i]);
    }

    for (unsigned int i = 0; i < tiles_.size(); i++)
    {
        renderer->draw(tiles_[i]);
    }

	for (std::vector<Enemy*>::iterator enemy = EnemyManager::getEnemies().begin(); enemy != EnemyManager::getEnemies().end(); enemy++)
	{
		renderer->draw(*enemy);
		for (unsigned int i = 0; i < (*enemy)->getProjectiles().size(); i++)
			renderer->draw((*enemy)->getProjectiles()[i]);
	}

    for (unsigned int i = 0; i < props_.size(); i++)
    {
        renderer->draw(&props_[i]);
    }

    if (upgrade_present_)
    {
        renderer->draw(&upgrade_model_);
    }

    if (show_upgrades_)
    {
        Upgrades::draw(renderer, upgrades_[0], player_->getPosition() + glm::vec3(4.0f, 2 * 1.8f, 2.0f));
        Upgrades::draw(renderer, upgrades_[1], player_->getPosition() + glm::vec3(4.0f, 1.8f, 2.0f));
        Upgrades::draw(renderer, upgrades_[2], player_->getPosition() + glm::vec3(4.0f, 0.0f, 2.0f));
        Upgrades::drawSelect(renderer, upgrades_[player_->getUpgradeSelectIndex()]);
    }

    // Debug
    /*
    debug_.setShape3D(player_->getHitbox(), ShaderManager::debug);
    renderer->draw(&debug_);

    for (unsigned int i = 0; i < collisions_.getOBBs().size(); i++)
    {
        debug_.setShape3D(collisions_.getOBBs()[i], ShaderManager::debug);
        renderer->draw(&debug_);
    }

	for (std::vector<Enemy*>::iterator enemy = EnemyManager::getEnemies().begin(); enemy != EnemyManager::getEnemies().end(); enemy++)
	{
		debug_.setShape3D((*enemy)->getHitbox(), ShaderManager::debug);
		renderer->draw(&debug_);

        (*enemy)->drawAttackDebug(renderer, &debug_);

        for (unsigned int i = 0; i < (*enemy)->getProjectiles().size(); i++)
        {
            debug_.setShape3D((*enemy)->getProjectiles()[i]->getHitbox(), ShaderManager::debug);
            renderer->draw(&debug_);
        }
	}
    for (unsigned int i = 0; i < player_->getProjectiles().size(); i++)
    {
        debug_.setShape3D(player_->getProjectiles()[i]->getHitbox(), ShaderManager::debug);
        renderer->draw(&debug_);
    }

    for (unsigned int i = 0; i < spikes_.size(); i++)
    {
        debug_.setShape3D(spikes_[i], ShaderManager::debug);
        renderer->draw(&debug_);
    }

    if (upgrade_present_)
    {
        debug_.setShape3D(upgrade_area_, ShaderManager::debug);
        renderer->draw(&debug_);
    }

    debug_.setShape3D(exit_area_, ShaderManager::debug);
    renderer->draw(&debug_);
    */
}

bool Level::canProgress()
{
	unsigned int enemies = static_cast<unsigned int>(floor(EnemyManager::getEnemies().size() * 0.7f));
	float key_ratio;

	if (enemies > 0)
	{
		key_ratio = player_->getKillNumber() / static_cast<float>(enemies);

		if (key_ratio >= 0.99999999999f && !played_key_sound_)
		{
			SoundManager::playSound(SoundManager::Proceed, 50);
			played_key_sound_ = true;
		}
	}
	else
	{
		key_ratio = 1.0f;
	}

	return (key_ratio >= 0.99999999999f);
}