#pragma once
#include "Enemy.hpp"

class Spearman : public Enemy
{
public:
	Spearman(Player& p) : Enemy(p) {};
	Spearman(glm::vec3 pos, Player& p) : Enemy(p) { setPosition(pos); };

	void initialize(cl::AssetCache* cache);
	void update(float dt);
	void processAnimations();
};
