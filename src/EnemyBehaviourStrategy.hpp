#pragma once
#include "Enemy.hpp"
#include "Player.hpp"

class EnemyBehaviourStrategy
{
public:
	virtual void move(Enemy& e, Player& p) = 0; // possibly should return a direction that the enemy should move
	virtual ~EnemyBehaviourStrategy() {};
	state state_;
	/*
	virtual void attack() = 0;
	virtual void die() = 0;
	*/
};

