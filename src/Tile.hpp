#pragma once
#include <Clone/3D.hpp>
#include "Collisions.hpp"

class Tile : public cl::Model
{
public:
    Tile();
    Tile(const Tile &obj);
    ~Tile();
    Tile& operator=(const Tile &obj);


    void initialize(const std::string &file_name, cl::AssetCache *cache);

    const std::vector<cl::OBB3D>& getCollisions() const;

private:
    Collisions collisions_;
};