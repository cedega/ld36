#include <Clone/OpenGL/OpenGLAPI.hpp>
#include "Engine.hpp"

int main(int argc, char *argv[])
{
    cl::OpenGLAPI::activate();
    Engine clone;
    clone.runGameLoop();
    return 0;
}
