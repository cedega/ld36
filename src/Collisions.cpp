#include "Collisions.hpp"
#define _USE_MATH_DEFINES
#include <math.h>
#include <Clone/Core.hpp>
#include <algorithm>
#include <string>

Collisions::Collisions()
{
    obbs_.reserve(256);
}

void Collisions::clear()
{
    obbs_.clear();
}

void Collisions::add(const cl::OBB3D &obb)
{
    obbs_.push_back(obb);
}

void Collisions::load(std::string contents, const glm::vec3 &offset)
{
    contents.erase(std::remove(contents.begin(), contents.end(), '\r'), contents.end());

    size_t start = 0;
    std::string line;

    glm::vec3 position;
    glm::vec3 radius;
    glm::vec3 rotation;

    bool first = true;
    int index = 0;
    int runs = 0;
    int box_index = 0;
    
    while (true)
    {
        size_t endline = contents.find('\n', start);
        line = contents.substr(start, endline - start);
        // cl::Log(cl::CL_LOG_INFO, "%s", line.c_str());

        if (runs == 0)
        {
            if (atoi(line.c_str()) == 0)
                break;

            obbs_.resize(atoi(line.c_str()));
        }
        else if (runs == 1)
        {
            // Nothing -- unused
        }
        else if (line.compare("END") == 0)
        {
            obbs_[box_index] = cl::OBB3D(position + offset, radius);
            obbs_[box_index].setRotation(rotation);
            // cl::Log(cl::CL_LOG_INFO, "POS: %s, RAD: %s", cl::toStringV3(position).c_str(), cl::toStringV3(radius).c_str());
            box_index++;

            index = 0;
            break;
        }
        else if (line.compare("BOX") == 0)
        {
            if (!first)
            {
                obbs_[box_index] = cl::OBB3D(position + offset, radius);
                obbs_[box_index].setRotation(rotation);
                // cl::Log(cl::CL_LOG_INFO, "POS: %s, RAD: %s", cl::toStringV3(position).c_str(), cl::toStringV3(radius).c_str());
                box_index++;
            }

            first = false;
            index = 0;
        }
        else
        {
            switch (index)
            {
            case 0: position.x = static_cast<float>(atof(line.c_str())); index++; break;
            case 1: position.y = static_cast<float>(atof(line.c_str())); index++; break;
            case 2: position.z = static_cast<float>(atof(line.c_str())); index++; break;
            case 3: radius.x =   static_cast<float>(atof(line.c_str())); index++; break;
            case 4: radius.y =   static_cast<float>(atof(line.c_str())); index++; break;
            case 5: radius.z =   static_cast<float>(atof(line.c_str())); index++; break;
            case 6: rotation.x = static_cast<float>(atof(line.c_str())); index++; break;
            case 7: rotation.y = static_cast<float>(atof(line.c_str())); index++; break;
            case 8: rotation.z = static_cast<float>(atof(line.c_str())); index++; break;
            default: break;
            }
        }

        start = endline + 1;
        runs++;
    } 
}

const std::vector<cl::OBB3D>& Collisions::getOBBs() const
{
    return obbs_;
}