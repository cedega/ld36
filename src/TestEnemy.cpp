#include "TestEnemy.hpp"
#include "ShaderManager.hpp"
#include "PatrolStrategy.hpp"
#include "ChaseStrategy.hpp"

void TestEnemy::initialize(cl::AssetCache* cache)
{
	// memes happen here
	cl::AssetLoader loader;
	cl::AssetLoader::TextureArguments texargs;
	texargs.loader_args = cl::AssetLoader::IR;
	texargs.min_filter = cl::Texture::Linear;
	texargs.mag_filter = cl::Texture::Linear;
	cl::Texture *texture = loader.loadTexture("data/ariaspritesheet.png", texargs, cache);
	cl::AnimatedSpriteSheet::initialize(texture, ShaderManager::shader2d, false, glm::ivec2(80, 80), 2);

	cl::AnimatedSpriteSheet::createAnimation("idle", 1, 12, 50 / 1000.0f);
	cl::AnimatedSpriteSheet::createAnimation("run", 13, 24, 50 / 1000.0f);
	cl::AnimatedSpriteSheet::createAnimation("hit", 32, 32, 100 / 1000.0f);
	cl::AnimatedSpriteSheet::createAnimation("death", 33, 49, 100 / 1000.0f);
	cl::AnimatedSpriteSheet::createAnimation("jump", 65, 65, 100 / 1000.0f);
	cl::AnimatedSpriteSheet::createAnimation("fall", 66, 66, 100 / 1000.0f);

	cl::AnimatedSpriteSheet::setLooping("crouch", false);
	cl::AnimatedSpriteSheet::setLooping("jump", false);
	cl::AnimatedSpriteSheet::setLooping("fall", false);

	cl::AnimatedSpriteSheet::loadAnimation("idle");

	hitbox_.radius = glm::vec3(0.5f, 1.0f, 0.5f);
	cl::AnimatedSpriteSheet::setScaling(glm::vec3(0.03f), true);

	// strats
	//PatrolStrategy p(glm::vec3(1.0f), glm::vec3(5.0f));
	speed_ = 2.0f;
	setJump(20.0f);
	setDamage(5.0f);
}

void TestEnemy::update(float dt)
{ 
	float minimumchasedistance = 5.0f;
	if (getStrategy(state::STRAT_CHASE))
	{
		if (glm::abs(getPosition().x - player_.getPosition().x) < minimumchasedistance )
		{ 
			setStrategy(state::STRAT_CHASE);
		}
		else if (getStrategy(state::STRAT_PATROL))
		{
			setStrategy(state::STRAT_PATROL);
		}
	}
	Enemy::update(dt);
}
