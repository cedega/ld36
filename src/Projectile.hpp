#pragma once
#include <Clone/2D/AnimatedSpriteSheet.hpp>
#include <Clone/Core.hpp>
#include <Clone/Physics/EulerPhysics.hpp>
#include <Clone/Geometry.hpp>
#include "Collisions.hpp"

class Player;
class Entity;

class Projectile : public cl::AnimatedSpriteSheet
{
friend class Engine;
public:
    Projectile();

    virtual void create(const glm::vec3 &position, const glm::vec3 &direction, bool target_enemy) = 0;
    virtual void processAnimations() = 0;
    void update(float dt);

    void setPosition(const glm::vec3 &pos);

    void setCollisions(Collisions *collisions);
    float getRotationFromDirection(const glm::vec3 &direction);

    void setSpeed(float speed);
    float getSpeed() const;

    void setDamage(int damage);
    int getDamage() const;

    const cl::OBB3D& getHitbox() const;
    bool isAlive() const;
    bool shouldDestroy() const;

protected:
    static cl::AssetCache *cache_;
    static Player *player_;
    Collisions *collisions_;
    cl::EulerPhysics physics_;
    cl::OBB3D hitbox_;
    glm::vec3 hitbox_offset_;
    glm::vec3 direction_;
    float speed_;
    int damage_;
    float life_tick_;
    float life_time_;
    bool alive_;
    bool destroy_;
    bool target_enemy_;
};