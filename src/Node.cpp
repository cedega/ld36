#include "Node.hpp"
std::vector<Node*>* Node::mNodes;

// add nodes based on the level
// could read from a script to improve iterative testing
void Node::initNodes(const int level)
{
	if (mNodes != nullptr)
	{
		mNodes->clear();
	}
	else
	{
		mNodes = new std::vector<Node*>();
	}
	switch (level)
	{
	case 1:
		// add nodes to mNodes
		break;
	}
}

Node::Node(glm::vec3 pos) :
	mPos(pos)
{ 
}

Node::~Node()
{ 
	// don't need to do shit lmaoooo
}

// iterate through all nodes, find the one that matches the 
// this probably shouldn't be called every frame in update, but the enemy class
// hasn't been implemented yet so i don't have state to modify
glm::vec3 Node::getNearestNode(const glm::vec3 enemyPos, const glm::vec3 playerPos)
{ 
	glm::vec3 nearestY(-1, -1, -1);
	float delta = 5.0f; // adjust as necessary
	if (!mNodes)
		return enemyPos; // :^)
	for (auto itr : *mNodes)
	{
		if (glm::abs(itr->mPos.y - enemyPos.y) < delta) // only one node on a given Y-level
		{
			nearestY = itr->mPos;
		}
	}
	return nearestY;
}