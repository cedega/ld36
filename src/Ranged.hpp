#pragma once
#include "Enemy.hpp"

class Ranged : public Enemy
{
public:
	Ranged(Player& p) : Enemy(p) {};
	Ranged(glm::vec3 pos, Player& p) : Enemy(p) { setPosition(pos); };

	void initialize(cl::AssetCache* cache);
	void update(float dt);
	void processAnimations();
private:
	float attack_tick_ = 0.6f;
	float attack_timer_ = 1.0f;
	float range_ = 5.0f;
};