#include "SoundManager.hpp"
#include <iostream>
cl::AssetCache *SoundManager::cache_ = NULL;



void SoundManager::initialize(cl::AssetCache *cache)
{
	cache_ = cache;

	// Preload sounds
	cl::AssetLoader loader;

	// Music
	loader.loadMusic(swizzleMusicEnum(Sand), cl::AssetLoader::None, cache_);
	loader.loadMusic(swizzleMusicEnum(Tech), cl::AssetLoader::None, cache_);
	loader.loadMusic(swizzleMusicEnum(Boss), cl::AssetLoader::None, cache_);

	// Sound Effects
	loader.loadSound("data/audio/sound/en_arrow1.ogg", cl::AssetLoader::None, cache_);
	loader.loadSound("data/audio/sound/en_arrow2.ogg", cl::AssetLoader::None, cache_);
	loader.loadSound("data/audio/sound/en_arrow3.ogg", cl::AssetLoader::None, cache_);
	loader.loadSound(swizzleSoundEnum(Jump), cl::AssetLoader::None, cache_);
	loader.loadSound("data/audio/sound/bark1.ogg", cl::AssetLoader::None, cache_);
	loader.loadSound("data/audio/sound/bark2.ogg", cl::AssetLoader::None, cache_);
	loader.loadSound("data/audio/sound/growl1.ogg", cl::AssetLoader::None, cache_);
	loader.loadSound("data/audio/sound/growl2.ogg", cl::AssetLoader::None, cache_);
	loader.loadSound(swizzleSoundEnum(Break), cl::AssetLoader::None, cache_);
	loader.loadSound(swizzleSoundEnum(Bat), cl::AssetLoader::None, cache_);
	loader.loadSound(swizzleSoundEnum(EnemyHit), cl::AssetLoader::None, cache_);
	loader.loadSound(swizzleSoundEnum(DogDeath), cl::AssetLoader::None, cache_);
	loader.loadSound(swizzleSoundEnum(BatDeath), cl::AssetLoader::None, cache_);
	loader.loadSound(swizzleSoundEnum(GirlDeath), cl::AssetLoader::None, cache_);
	loader.loadSound(swizzleSoundEnum(GirlHurt), cl::AssetLoader::None, cache_);
	loader.loadSound(swizzleSoundEnum(GuyDeath), cl::AssetLoader::None, cache_);
	loader.loadSound("data/audio/sound/guy_hurt1.ogg", cl::AssetLoader::None, cache_);
	loader.loadSound("data/audio/sound/guy_hurt2.ogg", cl::AssetLoader::None, cache_);
	loader.loadSound(swizzleSoundEnum(BossAttack), cl::AssetLoader::None, cache_);
	loader.loadSound(swizzleSoundEnum(BossImpact), cl::AssetLoader::None, cache_);
	loader.loadSound("data/audio/sound/boss_hurt1.ogg", cl::AssetLoader::None, cache_);
	loader.loadSound("data/audio/sound/boss_hurt2.ogg", cl::AssetLoader::None, cache_);
	loader.loadSound(swizzleSoundEnum(BossLaugh), cl::AssetLoader::None, cache_);
	loader.loadSound(swizzleSoundEnum(BossDie), cl::AssetLoader::None, cache_);
	loader.loadSound(swizzleSoundEnum(BossStep), cl::AssetLoader::None, cache_);
	loader.loadSound(swizzleSoundEnum(Proceed), cl::AssetLoader::None, cache_);
	loader.loadSound(swizzleSoundEnum(Upgrade), cl::AssetLoader::None, cache_);
	loader.loadSound(swizzleSoundEnum(Select), cl::AssetLoader::None, cache_);
}



void SoundManager::playSound(SoundEffect se, int volume)
{
	cl::AssetLoader loader;
	cl::Sound *sound = loader.loadSound(swizzleSoundEnum(se), cl::AssetLoader::None, cache_);
	sound->setVolume(volume);
	sound->play();
}

void SoundManager::playMusic(MusicEffect me, int volume)
{
	cl::AssetLoader loader;
	cl::Music *music = loader.loadMusic(swizzleMusicEnum(me), cl::AssetLoader::None, cache_);
	music->setVolume(volume);
	music->setLooping(true);
	music->play();

}

void SoundManager::stopMusic()
{
	cl::Music::stop();
}

std::string SoundManager::swizzleSoundEnum(SoundManager::SoundEffect se)
{
	if (se == EnergyArrow)
	{
		int picker = cl::random(1, 3);

		if (picker == 1) return "data/audio/sound/en_arrow1.ogg";
		else if (picker == 2) return "data/audio/sound/en_arrow2.ogg";
		else if (picker == 3) return "data/audio/sound/en_arrow2.ogg";
	}

	else if (se == Bark)
	{
		int picker = cl::random(1, 2);

		if (picker == 1) return "data/audio/sound/bark1.ogg";
		else if (picker == 2) return "data/audio/sound/bark2.ogg";
	}

	else if (se == Growl)
	{
		int picker = cl::random(1, 2);

		if (picker == 1) return "data/audio/sound/growl1.ogg";
		else if (picker == 2) return "data/audio/sound/growl2.ogg";
	}

	else if (se == GuyHurt)
	{
		int picker = cl::random(1, 2);

		if (picker == 1) return "data/audio/sound/guy_hurt1.ogg";
		else if (picker == 2) return "data/audio/sound/guy_hurt2.ogg";
	}
	else if (se ==BossHurt)
	{
		int picker = cl::random(1, 2);

		if (picker == 1) return "data/audio/sound/boss_hurt1.ogg";
		else if (picker == 2) return "data/audio/sound/boss_hurt2.ogg";
	}

	else
	{
		switch (se)
		{
		case SoundManager::EnergyArrow: return "data/audio/dog.ogg";
		case SoundManager::Jump: return "data/audio/sound/jump.ogg";
		case SoundManager::Break: return "data/audio/sound/ob_break.ogg";
		case SoundManager::Bat: return "data/audio/sound/bat.ogg";
		case SoundManager::EnemyHit: return "data/audio/sound/enemy_hit.ogg";
		case SoundManager::DogDeath: return "data/audio/sound/dog_death.ogg";
		case SoundManager::BatDeath: return "data/audio/sound/bat_death.ogg";
		case SoundManager::GirlDeath: return "data/audio/sound/girl_death.ogg";
		case SoundManager::GirlHurt: return "data/audio/sound/girl_hurt.ogg";
		case SoundManager::GuyDeath: return "data/audio/sound/guy_death.ogg";
		case SoundManager::BossAttack: return "data/audio/sound/boss_attack.ogg";
		case SoundManager::BossImpact: return "data/audio/sound/boss_impact.ogg";
		case SoundManager::BossLaugh: return "data/audio/sound/boss_laugh.ogg";
		case SoundManager::BossDie: return "data/audio/sound/boss_die.ogg";
		case SoundManager::BossStep: return "data/audio/sound/boss_step.ogg";
		case SoundManager::Proceed: return "data/audio/sound/proceed.ogg";
		case SoundManager::Upgrade: return "data/audio/sound/upgrade.ogg";
		case SoundManager::Select: return "data/audio/sound/select.ogg";
		default: return "";
		}
	}

    return "";
}

std::string SoundManager::swizzleMusicEnum(SoundManager::MusicEffect me)
{
	switch (me)
	{
	case SoundManager::Sand: return "data/audio/music/sand_song.ogg";
	case SoundManager::Tech: return "data/audio/music/tech_song.ogg";
	case SoundManager::Boss: return "data/audio/music/boss_song.ogg";
	default: return "";
	}
}
