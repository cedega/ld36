#pragma once
#include <string>
#include <vector>
#include <Clone/Geometry.hpp>

class Collisions
{
public:
    Collisions();

    void clear();
    void add(const cl::OBB3D &obb);
    void load(std::string contents, const glm::vec3 &offset);

    const std::vector<cl::OBB3D>& getOBBs() const;

private:
    std::vector<cl::OBB3D> obbs_;
};
