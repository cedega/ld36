#pragma once
#include "EnemyBehaviourStrategy.hpp"

class BossStrategy : public EnemyBehaviourStrategy
{
public:
	void move(Enemy& e, Player& p);
	BossStrategy();
};