#pragma once
#include "Enemy.hpp"

class Boss : public Enemy
{
public:
	Boss(Player& p) : Enemy(p) {};
	Boss(glm::vec3 pos, Player& p) : Enemy(p) { setPosition(pos); };

	void initialize(cl::AssetCache* cache);
	void update(float dt);
	void processAnimations();

    bool getAttackBox(cl::OBB3D *output);
    void drawAttackDebug(cl::Renderer *renderer, cl::DebugRenderObject *debug);
};
