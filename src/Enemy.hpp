#pragma once
#include <Clone/Core.hpp>
#include <Clone/2D/AnimatedSpriteSheet.hpp>
#include <Clone/Physics/EulerPhysics.hpp>
#include <Clone/Geometry.hpp>
#include "Entity.hpp"
#include "Collisions.hpp"
#include "Player.hpp"
#include "SoundManager.hpp"
#include <Clone/Graphics.hpp>

class Node;
class EnemyBehaviourStrategy;
enum state
{
	STRAT_PATROL = 0,
	STRAT_CHASE,
	STRAT_ATTACK,
	STAND
};

class Enemy : public Entity
{
protected:
	struct EnemyInput
	{
		bool up;
		bool left;
		bool right;
		bool down;
		bool attack;
		
		EnemyInput() :
			up(false),
			left(false),
			right(false),
			down(false),
			attack(false)
		{
		}
	};
public:
	Enemy(const glm::vec3& pos, Player& p);
	Enemy(Player& p);
	~Enemy();

	void setPosition(const glm::vec3& pos);
	void setCollisions(Collisions *collisions);
	void setStrategy(state s);
	void setPlayer(const Player& p);
	void addStrategy(EnemyBehaviourStrategy* strat);
	EnemyBehaviourStrategy* getStrategy(state s);

	cl::EulerPhysics& getPhysics();

	virtual void initialize(cl::AssetCache *cache) = 0;
	virtual void update(float dt);

	EnemyInput input_;

	virtual void onDeath();

	virtual bool isPressingLeft() { return input_.left; };
	virtual bool isPressingRight() { return input_.right; };
	virtual bool isPressingJump() { return input_.up;  };
    virtual void processAnimations();

    virtual bool getAttackBox(cl::OBB3D *output) { return false; }
    virtual void drawAttackDebug(cl::Renderer *renderer, cl::DebugRenderObject *debug) {}

    static void setStatScale(float scale);
    static float getStatScale();

protected:
    static float scaling_;
	Player& player_;
	EnemyBehaviourStrategy* strategy_;
	std::vector<EnemyBehaviourStrategy*> strategies_;
	float damage_;
    float attack_damage_;
	float damageTick_;
	bool sound_is_not_playing_;
	bool death_sound_played_;
	virtual void setDamage(float dmg) { damage_ = dmg; };
	virtual void playAttackSound() {};
};

