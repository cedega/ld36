#pragma once
#include "Enemy.hpp"

class Bat : public Enemy
{
public:
	Bat(glm::vec3 pos, Player& p) : Enemy(p) { setPosition(pos); };
	Bat(Player& p) : Enemy(p) {};

	void initialize(cl::AssetCache* cache);
	void update(float dt);
	void processAnimations();
};
