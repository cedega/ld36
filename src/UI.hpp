#pragma once
#include <Clone/Graphics.hpp>
#include <Clone/2D.hpp>

class Player;

class UI
{
public:
    UI();

    void initialize(cl::AssetCache *cache);
    void render(cl::Renderer *renderer);
    void update(Player *player, float dt);

private:
    cl::Image health_;
    cl::Image health_max_;
	cl::Image key_;
	cl::Image key_max_;
};
