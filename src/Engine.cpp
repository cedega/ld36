#include "Engine.hpp"
#include "ShaderManager.hpp"
#include "SoundManager.hpp"
#include "Upgrades.hpp"

Engine::Engine() :
    fade_tick_(0.0f),
    fullscreen_pressed_(false)
{
    if (!cl::Engine::initialize())
        return;

    // cl::AssetLoader::setEmscriptenPath("");
    // cl::ZipIO::setZipFile("bin/emscripten/data/data.zip");
    // cl::AssetLoader::loadAllFromZip(true);

    Map::player_ = &player_;
    Level::player_ = &player_;
    Level::cache_ = &cache_;
    Level::camera_ = &camera_;
    Projectile::cache_ = &cache_;
    Projectile::player_ = &player_;

    cl::Engine::createWindow("Pyramid", glm::uvec2(1280, 720), 0);
    cl::Engine::setVerticalSync(true);
	cl::Engine::setMaxAudioChannels(64);

    cl::EulerPhysics::setDefaultGravity(glm::vec3(0.0f, -35.0f, 0.0f));
    cl::GraphicsAPI::getInstance()->setClearColour(glm::vec4(0.5f, 0.5f, 0.5f, 1.0f));
    ShaderManager::initialize();
	SoundManager::initialize(&cache_);
    Upgrades::initialize(&cache_);
    player_.initialize(&cache_);
    main_menu_.initialize(&cache_);
    ui_.initialize(&cache_);

    camera_.setProjectionMatrix(glm::perspective(glm::radians(35.0f), cl::Engine::getAspectRatio(), 1.0f, 100.0f));
    camera_.setPositionOffset(glm::vec3(0.0f, 1.0f, 25.0f));
    camera_.snapTo(glm::vec3(0.0f));

    camera2d_.setProjectionMatrix(glm::ortho(0.0f, 1920.0f, 1080.0f, 0.0f, -1.0f, 1.0f));

	SoundManager::playMusic(SoundManager::Sand, 15);

    cl::AssetLoader loader;
    cl::AssetLoader::TextureArguments texargs;
    texargs.min_filter = cl::Texture::Linear;
    texargs.mag_filter = cl::Texture::Linear;
    cl::Texture *texture = loader.loadTexture("data/youwin.png", texargs, &cache_);
    win_image_.initialize(texture, ShaderManager::shader2d, true);
    win_image_.setTextureRect(glm::vec4(0, 0, 1920, 1080));
    win_image_.setPosition(glm::vec3(0.0f), cl::Image::TopLeft, true);

    map_.initialize();

    // Upgrades::add(Upgrades::MultiShot);
}

Engine::~Engine()
{
    ShaderManager::destroy();
    Upgrades::destroy();
    cache_.clear();
}

void Engine::events(const SDL_Event &event)
{
    if (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_RESIZED)
    {
        camera_.setProjectionMatrix(glm::perspective(glm::radians(35.0f), cl::Engine::getAspectRatio(), 1.0f, 100.0f));
    }
    else if (event.type == SDL_QUIT)
    {
        cl::Engine::killGameLoop();
    }
}

void Engine::logic(float dt)
{
    cl::Keys keys = cl::Input::getKeyboardState();

    if (keys[CL_KEY_LALT] && keys[CL_KEY_ENTER] && !fullscreen_pressed_)
    {
        setFullscreen(!getFullscreen());
        cl::Engine::setVerticalSync(true);
    }

    fullscreen_pressed_ = keys[CL_KEY_LALT] && keys[CL_KEY_ENTER];

    if (main_menu_.inMenu())
    {
        main_menu_.update(dt);
    }
    else
    {
        map_.update(dt);
        ui_.update(&player_, dt);
        camera_.moveTo(player_.getPhysicsPosition(), dt);
    }

    if (map_.hasWon())
        fade_tick_ += dt * 0.5f;
}

void Engine::render(cl::Renderer *renderer)
{
    if (main_menu_.inMenu())
    {
        camera2d_.updateViewMatrix(1.0f);
        renderer->setCamera(&camera2d_);
        main_menu_.render(renderer);
    }
    else
    {
        camera_.updateViewMatrix(renderer->getInterpolation());
        renderer->setCamera(&camera_);
        map_.render(renderer);

        renderer->setCamera(&camera2d_);
        ui_.render(renderer);

        map_.renderFade(renderer);
    }

    if (map_.hasWon())
    {
        if (fade_tick_ > 1.0f)
            fade_tick_ = 1.0f;

        win_image_.setColour(glm::vec4(1.0f, 1.0f, 1.0f, fade_tick_));

        Entity::setDisabled(true);
        renderer->setCamera(&camera2d_);
        renderer->draw(&win_image_);
    }
}

