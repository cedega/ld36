BINARY_NAME=Pyramid
INCLUDE=../clone/include/
CFLAGS=-O3
FRAMEWORKS=
PLUGINS=
PLUGIN_FRAMEWORKS=

EMSCRIPTEN_POST=
EMSCRIPTEN_PRELOAD=bin/emscripten/data
EMSCRIPTEN_MEMORY=1006777216

ifneq (,$(findstring emcc,$(CC)))

	MESSAGE="Making Target: Web"
	COMPILE=../clone/lib/emscripten/
	LIBS=-lclone -s TOTAL_MEMORY=$(EMSCRIPTEN_MEMORY) -s USE_SDL=2 -s USE_SDL_IMAGE=2 -s USE_LIBPNG=1 -s USE_SDL_TTF=2 -s USE_FREETYPE=1 -s USE_SDL_NET=2 -lSDL2_mixer -s USE_OGG=1 -s USE_VORBIS=1 -s USE_ZLIB=1
	BINARY=bin/emscripten/$(BINARY_NAME).html
	BUILD_DIR=build-web
	PLUGIN_COMPILE = $(foreach plugin,$(PLUGINS),-L"../$(plugin)/lib/emscripten/")
	CFLAGS:=$(subst -g,,$(CFLAGS))
	EMSCRIPTEN_POST:=--preload-file $(EMSCRIPTEN_PRELOAD) --use-preload-plugins --separate-asm --shell-file bin/emscripten/emscripten-shell.html

else ifeq ($(OS),Windows_NT)

	MESSAGE="Making Target: Windows"
	COMPILE=../clone/lib/windows/mingw/
	LIBS=-lclone -lmingw32 -lSDL2main -lSDL2 -lSDL2_image -lSDL2_ttf -lSDL2_net -lSDL2_mixer -lglu32 -lopengl32 -lminizip -lz -lm -Wall -static-libgcc -static-libstdc++
	BINARY=bin/windows/$(BINARY_NAME).exe
	BUILD_DIR=build
	PLUGIN_COMPILE = $(foreach plugin,$(PLUGINS),-L"../$(plugin)/lib/windows/mingw/")

else ifeq ($(shell uname), Darwin)

	MESSAGE="Making Target: Mac OSX"
	COMPILE=../clone/lib/mac/
	LIBS=-clone -framework SDL2 -framework SDL2_image -framework SDL2_mixer -framework SDL2_ttf -framework SDL2_net -lminizip -lz
	FRAMEWORKS="-F../clone/lib/mac/"
	BINARY=bin/mac/$(BINARY_NAME)
	BUILD_DIR=build
	PLUGIN_COMPILE=$(foreach plugin,$(PLUGINS),-L"../$(plugin)/lib/mac/")
	PLUGIN_FRAMEWORKS=$(foreach plugin,$(PLUGINS),"-F../$(plugin)/lib/mac")

else

	MESSAGE="Making Target: Linux"
	COMPILE=../clone/lib/linux/
	LIBS=-lclone -lSDL2 -lSDL2_image -lSDL2_ttf -lSDL2_net -lSDL2_mixer -lGLU -lGL -lminizip -lz -lm -pthread -ldl -lfreetype -Wall
	BINARY=bin/linux/$(BINARY_NAME)
	BUILD_DIR=build
	PLUGIN_COMPILE = $(foreach plugin,$(PLUGINS),-L"../$(plugin)/lib/linux/")

endif

PLUGIN_INCLUDE := $(foreach plugin,$(PLUGINS),-I"../$(plugin)/include/")
PLUGIN_OBJ := $(foreach plugin,$(PLUGINS),$(wildcard ../$(plugin)/$(BUILD_DIR)/*.o ../$(plugin)/$(BUILD_DIR)/*/*.o))
PLUGIN_OBJ := $(PLUGIN_OBJ) $(foreach plugin,$(PLUGINS),$(wildcard ../$(plugin)/$(BUILD_DIR)/*.opp ../$(plugin)/$(BUILD_DIR)/*/*.opp))
PLUGIN_OBJ := $(PLUGIN_OBJ) $(foreach plugin,$(PLUGINS),$(wildcard ../$(plugin)/$(BUILD_DIR)/*.bc ../$(plugin)/$(BUILD_DIR)/*/*.bc))

ifneq (,$(findstring emcc,$(CC)))
PLUGIN_LIBS := $(foreach plugin,$(PLUGINS),$(shell cat ../$(plugin)/LibsWeb.txt))
else
PLUGIN_LIBS := $(foreach plugin,$(PLUGINS),$(shell cat ../$(plugin)/Libs.txt))
endif

srcs := $(wildcard src/*.cpp src/*/*.cpp src/*/*/*.cpp)
csrcs := $(wildcard src/*.c src/*/*.c src/*/*/*.c)

ifneq (,$(findstring emcc,$(CC)))

objs := $(srcs:.cpp=.bc)
cobjs := $(csrcs:.c=.o)

buildobjs := $(subst src/,,$(foreach object,$(objs),$(BUILD_DIR)/$(object)))
buildcobjs := $(subst src/,,$(foreach object,$(cobjs),$(BUILD_DIR)/$(object)))
builddeps := $(buildobjs:.bc=.d) $(buildcobjs:.o=.d)

else

objs := $(srcs:.cpp=.opp)
cobjs := $(csrcs:.c=.o)

buildobjs := $(subst src/,,$(foreach object,$(objs),$(BUILD_DIR)/$(object)))
buildcobjs := $(subst src/,,$(foreach object,$(cobjs),$(BUILD_DIR)/$(object)))
builddeps := $(buildobjs:.opp=.d) $(buildcobjs:.o=.d)

endif

dirs := $(foreach sr,$(srcs),$(BUILD_DIR)/$(sr))
dirs := $(dirs) $(foreach sr,$(csrcs),$(BUILD_DIR)/$(sr))
dirs := $(sort $(foreach dr,$(subst src/,,$(dirs)),$(dir $(dr))))

all: dir bin

dir:
	mkdir -p $(dirs)

bin: $(buildobjs) $(buildcobjs)
	@echo $(MESSAGE)
	$(CXX) $(CFLAGS) $(buildobjs) $(buildcobjs) $(PLUGIN_OBJ) -I"$(INCLUDE)" $(PLUGIN_INCLUDE) -L"$(COMPILE)" $(PLUGIN_COMPILE) $(FRAMEWORKS) $(PLUGIN_FRAMEWORKS) $(LIBS) $(PLUGIN_LIBS) -o $(BINARY) $(EMSCRIPTEN_POST)

$(BUILD_DIR)/%.o: src/%.c
	$(CC) $(CFLAGS) -I"$(INCLUDE)" $(PLUGIN_INCLUDE) -L"$(COMPILE)" -MMD -MP -w -c $< -o $(subst src/,,$@)

$(BUILD_DIR)/%.opp: src/%.cpp
	$(CXX) $(CFLAGS) -I"$(INCLUDE)" $(PLUGIN_INCLUDE) -L"$(COMPILE)" -MMD -MP -Wall -c $< -o $(subst src/,,$@)

$(BUILD_DIR)/%.bc: src/%.cpp
	$(CXX) $(CFLAGS) -I"$(INCLUDE)" $(PLUGIN_INCLUDE) -L"$(COMPILE)" -MMD -MP -Wall -c $< -o $(subst src/,,$@)

clean:
	rm -rf $(BUILD_DIR)/*

-include $(builddeps)
